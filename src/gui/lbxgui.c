/*
 *  2ooM: The Master of Orion II Reverse Engineering Project
 *  Graphical tool for inspecting LBX archives.
 *  Copyright © 2010, 2014, 2024 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <config.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <gtk/gtk.h>

#include "lbx.h"
#include "image.h"
#include "lbxgui.h"

#include "bg.xbm"

static GtkTreeStore *archives;
static GtkBuilder *builder;

static GtkWidget *canvas;

static cairo_surface_t *framebuf;
static cairo_pattern_t *bg_pattern;
static gboolean bg_stipple;

static struct lbx_image *image;
static struct lbx_colour palette[256];

/* We support stacking up to three palettes, with each superseding the last. */
static struct lbx_colour palette_external[256];
static struct lbx_colour palette_internal[256];
static struct lbx_colour palette_override[256];

static void refresh_palette(void)
{
	memcpy(palette, palette_external, sizeof palette);
	lbxgui_stack_palette(palette, palette_internal);
	lbxgui_stack_palette(palette, palette_override);

	if (framebuf)
		lbxgui_render_restart(framebuf);
}

void play_toggled(GtkToggleButton *button, gpointer data)
{
	gboolean active = gtk_toggle_button_get_active(button);
	GtkWidget *spin;

	if (active && !image) {
		gtk_toggle_button_set_active(button, FALSE);
		return;
	}

	spin = GTK_WIDGET(gtk_builder_get_object(builder, "framespin"));
	gtk_widget_set_sensitive(spin, !active);

	if (active) {
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(spin), 0);
	}
}

void set_frame(GtkSpinButton *spin, gpointer data)
{
	unsigned frame = gtk_spin_button_get_value_as_int(spin);

	if (image)
		lbxgui_render_argb(framebuf, image, frame, palette);
	gdk_window_invalidate_rect(canvas->window, NULL, FALSE);
}

static void redraw_image(void)
{
	GtkSpinButton *spin;

	refresh_palette();

	spin = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "framespin"));
	set_frame(spin, NULL);
}

static void tick(void *p, double delta)
{
	static double elapsed = 0;
	double seconds_per_frame = 1.0/15;
	GtkSpinButton   *spin;
	GtkToggleButton *play;
	unsigned frame, newframe;

	if (!image)
		return;

	spin = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "framespin"));
	play = GTK_TOGGLE_BUTTON(gtk_builder_get_object(builder, "playbutton"));

	frame = gtk_spin_button_get_value_as_int(spin);
	if (!gtk_toggle_button_get_active(play))
		return;

	elapsed += delta;
	newframe = frame;
	while (elapsed > seconds_per_frame) {
		elapsed -= seconds_per_frame;

		if (++newframe >= image->frames) {
			if (image->leadin == image->frames - 1) {
				gtk_toggle_button_set_active(play, FALSE);
				break;
			}

			newframe = image->leadin;
		}
	}

	if (frame != newframe)
		gtk_spin_button_set_value(spin, newframe);
}

static gboolean timeout(gpointer data)
{
	static double lasttime = INFINITY;
	static GTimer *timer;

	if (isinf(lasttime)) {
		timer = g_timer_new();
		if (timer) {
			lasttime = 0;
		}
	} else {
		double time = g_timer_elapsed(timer, NULL);

		tick(data, time - lasttime);
		lasttime = time;
	}

	return TRUE;
}

gboolean canvas_expose(GtkWidget *canvas, GdkEventExpose *event, gpointer data)
{
	cairo_t *cr;

	cr = gdk_cairo_create(canvas->window);

	if (bg_stipple) {
		cairo_set_source_rgb(cr, 0.6, 0.6, 0.6);
		gdk_cairo_rectangle(cr, &event->area);
		cairo_fill(cr);

		cairo_set_source_rgb(cr, 0.3, 0.3, 0.3);
		gdk_cairo_rectangle(cr, &event->area);
		cairo_mask(cr, bg_pattern);
	} else {
		cairo_set_source_rgb(cr, 0, 0, 0);
		gdk_cairo_rectangle(cr, &event->area);
		cairo_fill(cr);
	}

	cairo_set_source_surface(cr, framebuf, 0, 0);
	gdk_cairo_rectangle(cr, &event->area);
	cairo_fill(cr);

	cairo_destroy(cr);

	return TRUE;
}

static int alloc_framebuffer(struct lbx_image *img)
{
	GtkSpinButton *spin;

	cairo_surface_destroy(framebuf);
	framebuf = cairo_image_surface_create(CAIRO_FORMAT_ARGB32,
	                                      img->width, img->height);

	spin = GTK_SPIN_BUTTON(gtk_builder_get_object(builder, "framespin"));
	gtk_spin_button_set_range(spin, 0, img->frames-1);
	gtk_spin_button_set_value(spin, 0);

	gtk_widget_set_size_request(canvas, img->width, img->height);
	return 0;
}

static int close_image(void *handle)
{
	lbx_file_close(handle);
	return 0;
}

static struct lbx_image *load_lbx_image(struct lbx *archive, unsigned index)
{
	LBXfile *file;
	struct lbx_image *image;

	file = lbx_file_open(archive, index);
	g_return_val_if_fail(file, NULL);

	image = lbx_img_open(file, &lbx_arch_fops, close_image);
	if (!image)
		lbx_file_close(file);
	return image;
}

void set_override(GtkComboBox *combo)
{
	GtkTreeIter iter;
	gpointer lbx;
	guint    index;
	struct lbx_image *img;

	memset(palette_override, 0, sizeof palette_override);
	if (!gtk_combo_box_get_active_iter(combo, &iter))
		return;

	gtk_tree_model_get(GTK_TREE_MODEL(archives), &iter,
		1, &lbx,
		2, &index,
		-1);

	img = load_lbx_image(lbx, index);
	if (img) {
		lbx_img_getpalette(img, palette_override);
		lbx_img_close(img);
		redraw_image();
	}
}

void set_palette(GtkComboBox *combo)
{
	GtkTreeIter iter;
	gpointer lbx;
	guint    index;
	LBXfile  *f;

	memset(palette_external, 0, sizeof palette_external);
	if (!gtk_combo_box_get_active_iter(combo, &iter))
		return;

	gtk_tree_model_get(GTK_TREE_MODEL(archives), &iter,
		1, &lbx,
		2, &index,
		-1);

	f = lbx_file_open(lbx, index);
	if (f) {
		if (lbx_img_loadpalette(f, &lbx_arch_fops, palette_external))
			memset(palette_external, 0, sizeof palette_external);
		lbx_file_close(f);
	}

	redraw_image();
}

void set_image(GtkComboBox *combo)
{
	GtkTreeIter iter;
	gpointer lbx;
	guint    index;
	struct lbx_image *img;

	if (image) {
		lbx_img_close(image);
		image = NULL;
	}

	gtk_widget_set_size_request(canvas, -1, -1);
	memset(palette_internal, 0, sizeof palette_internal);
	if (!gtk_combo_box_get_active_iter(combo, &iter))
		return;

	gtk_tree_model_get(GTK_TREE_MODEL(archives), &iter,
		1, &lbx,
		2, &index,
		-1);

	img = load_lbx_image(lbx, index);
	if (img) {
		if (lbx_img_getpalette(img, palette_internal) == -1) {
			puts("crap");
			lbx_img_close(img);
		}

		alloc_framebuffer(img);
	}

	image = img;
	redraw_image();
}

void set_background(GtkCheckMenuItem *item, gpointer data)
{
	bg_stipple = gtk_check_menu_item_get_active(item);
	redraw_image();
}

void show_about(GtkWidget *widget)
{
	GtkWindow *parent = GTK_WINDOW(gtk_widget_get_toplevel(widget));

	gtk_show_about_dialog(parent,
		"name", g_get_application_name(),
		"version", PACKAGE_VERSION,
		"copyright", "Copyright \xc2\xa9 2010 Nick Bowler",
		"website", "http://toom.sourceforge.net/",
		NULL);
}

static int load_archive(const char *path)
{
	GtkTreeIter iter1, iter2;
	struct lbx *lbx;
	gchar *basename;
	unsigned i;

	lbx = lbx_fopen(path);
	if (!lbx)
		return -1;

	basename = g_path_get_basename(path);
	gtk_tree_store_append(archives, &iter1, NULL);
	gtk_tree_store_set(archives, &iter1,
		0, basename,
		1, (gpointer)lbx,
		-1);
	g_free(basename);

	for (i = 0; i < lbx->nfiles; i++) {
		struct lbx_statbuf stat;

		lbx_file_stat(lbx, i, &stat);
		gtk_tree_store_append(archives, &iter2, &iter1);
		gtk_tree_store_set(archives, &iter2,
			0, stat.name,
			1, (gpointer)lbx,
			2, (guint)i,
			-1);
	}

	return 0;
}

static void load_error(const char *name, GtkWindow *window)
{
	GtkWidget *dialog;

	dialog = gtk_message_dialog_new(window,
		GTK_DIALOG_MODAL,
		GTK_MESSAGE_ERROR,
		GTK_BUTTONS_OK,
		"Failed to open %s.  Decent error messages coming eventually.",
		name);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

void open_archive(GtkWidget *widget)
{
	GtkWindow *window = GTK_WINDOW(gtk_widget_get_toplevel(widget));
	GtkFileFilter *lbxfilter, *allfilter;
	GtkFileChooser *chooser;
	GtkTreeIter iter;

	chooser = GTK_FILE_CHOOSER(gtk_file_chooser_dialog_new(
		"Load Archive",
		window,
		GTK_FILE_CHOOSER_ACTION_OPEN,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
		NULL));

	lbxfilter = gtk_file_filter_new();
	gtk_file_filter_set_name(lbxfilter, "LBX Archives (*.lbx)");
	gtk_file_filter_add_pattern(lbxfilter, "*.lbx");
	gtk_file_filter_add_pattern(lbxfilter, "*.LBX");

	allfilter = gtk_file_filter_new();
	gtk_file_filter_set_name(allfilter, "All Files");
	gtk_file_filter_add_pattern(allfilter, "*");

	gtk_file_chooser_add_filter(chooser, lbxfilter);
	gtk_file_chooser_add_filter(chooser, allfilter);

	while (gtk_dialog_run(GTK_DIALOG(chooser)) == GTK_RESPONSE_ACCEPT) {
		gchar *name = gtk_file_chooser_get_filename(chooser);

		if (load_archive(name) == 0) {
			g_free(name);
			break;
		}

		load_error(name, GTK_WINDOW(chooser));
		g_free(name);
	}

	gtk_widget_destroy(GTK_WIDGET(chooser));
}

static void clear_combobox(GtkEntry *entry, GtkEntryIconPosition pos,
                           GdkEvent *event, gpointer data)
{
	GtkComboBox *combo = data;

	gtk_entry_set_text(entry, "");
	gtk_combo_box_set_active(combo, -1);
	redraw_image();
}

static void init_combobox(GtkBuilder *builder, const char *name)
{
	GtkComboBox *combo;
	GtkEntry *entry;

	combo = GTK_COMBO_BOX(gtk_builder_get_object(builder, name));
	gtk_combo_box_set_model(combo, GTK_TREE_MODEL(archives));
	gtk_combo_box_entry_set_text_column(GTK_COMBO_BOX_ENTRY(combo), 0);

	entry = GTK_ENTRY(gtk_bin_get_child(GTK_BIN(combo)));
	g_signal_connect(G_OBJECT(entry), "icon-press",
		G_CALLBACK(clear_combobox), combo);
	gtk_entry_set_icon_from_stock(entry, GTK_ENTRY_ICON_SECONDARY,
		GTK_STOCK_CLEAR);
}

static void init_interface(void)
{
	GtkTreeViewColumn *column;
	GtkCellRenderer *renderer;
	GtkTreeView *tree;

	renderer = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new();
	gtk_tree_view_column_pack_start(column, renderer, TRUE);
	gtk_tree_view_column_add_attribute(column, renderer, "text", 0);

	tree = GTK_TREE_VIEW(gtk_builder_get_object(builder, "treeview"));
	gtk_tree_view_set_model(tree, GTK_TREE_MODEL(archives));
	gtk_tree_view_append_column(tree, column);

	init_combobox(builder, "palettechooser");
	init_combobox(builder, "imagechooser");
	init_combobox(builder, "overchooser");
}

/*
 * Reverse the bits in each byte of a 32-bit word.
 */
static guint32 reverse4b(guint32 v)
{
	v = ((v >> 1) & 0x55555555) | ((v & 0x55555555) << 1);
	v = ((v >> 2) & 0x33333333) | ((v & 0x33333333) << 2);
	v = ((v >> 4) & 0x0f0f0f0f) | ((v & 0x0f0f0f0f) << 4);
	return v;
}

static cairo_surface_t *
xbm_to_cairo(const void *xbmdata, int width, int height)
{
	const unsigned char *data = xbmdata;
	unsigned char *sdata;
	cairo_surface_t *s;
	int x, y, stride;

	s = cairo_image_surface_create(CAIRO_FORMAT_A1, width, height);
	if (cairo_surface_status(s) != CAIRO_STATUS_SUCCESS)
		return s;

	sdata = cairo_image_surface_get_data(s);
	stride = cairo_image_surface_get_stride(s);
	assert(stride % 4 == 0);
	width = (width + 7) / 8;

	cairo_surface_flush(s);
	for (y = 0; y < height; y++) {
		/*
		 * Cairo is nuts.  The mapping of bits to pixels in the A1
		 * format actually depends on the host *byte* ordering, so
		 * we actually need these two separate cases.
		 */
		if (G_BYTE_ORDER == G_LITTLE_ENDIAN) {
			/* Easy, XBM and Cairo match in this case */
			memcpy(sdata, data, width);
		} else {
			/* No such luck, we have to convert manually */
			for (x = 0; x < width; x += 4) {
				guint32 word = 0;

				memcpy(&word, data+x, MIN(4, width-x));
				word = reverse4b(word);
				memcpy(sdata+x, &word, 4);
			}
		}

		sdata += stride;
		data += width;
	}
	cairo_surface_mark_dirty(s);

	return s;
}

static void init_background(GdkDrawable *drawable)
{
	cairo_surface_t *bg;
	GtkWidget *check;

	bg = xbm_to_cairo(bg_bits, bg_width, bg_height);
	bg_pattern = cairo_pattern_create_for_surface(bg);
	cairo_pattern_set_extend(bg_pattern, CAIRO_EXTEND_REPEAT);
	cairo_surface_destroy(bg);

	check = GTK_WIDGET(gtk_builder_get_object(builder, "menu-background"));
	gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(check), TRUE);
	set_background(GTK_CHECK_MENU_ITEM(check), NULL);
}

int main(int argc, char **argv)
{
	extern char lbxgui_glade[];
	GtkWidget *window;
	GError *err = NULL;

	if (!gtk_init_with_args(&argc, &argv, NULL, NULL, NULL, &err)) {
		fprintf(stderr, "%s\n", err->message);
		return EXIT_FAILURE;
	}

	archives = gtk_tree_store_new(3,
		G_TYPE_STRING, G_TYPE_POINTER, G_TYPE_UINT);
	while (*argv)
		load_archive(*argv++);

	builder = gtk_builder_new();
	if (!gtk_builder_add_from_string(builder, lbxgui_glade, -1, &err)) {
		fprintf(stderr, "%s\n", err->message);
		return EXIT_FAILURE;
	}

	window = GTK_WIDGET(gtk_builder_get_object(builder, "mainwindow"));
	canvas = GTK_WIDGET(gtk_builder_get_object(builder, "canvas"));

	init_interface();
	gtk_builder_connect_signals(builder, window);

	g_timeout_add_full(G_PRIORITY_DEFAULT_IDLE, 10, timeout, NULL, NULL);
	gtk_widget_show_all(window);

	init_background(canvas->window);
	gtk_main();
}
