#ifndef LBXGUI_H_
#define LBXGUI_H_

#include "image.h"

int lbxgui_render_argb(cairo_surface_t *dst, struct lbx_image *img,
                       unsigned frame, const struct lbx_colour *palette);

void lbxgui_render_restart(cairo_surface_t *dst);
void lbxgui_stack_palette(struct lbx_colour *dst, const struct lbx_colour *src);

#endif
