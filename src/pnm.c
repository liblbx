/*
 * 2ooM: The Master of Orion II Reverse Engineering Project
 * Netpbm output routines for lbximg extration.
 *
 * Copyright © 2013-2014, 2021, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <limits.h>
#include <errno.h>

#include "image.h"
#include "tools.h"
#include "imgoutput.h"
#include "c99types.h"

/*
 * Format a 6-bit RGB values for PPM (P3) format.  This is a plain-text decimal
 * encoding using ASCII digits. The acceptable values for each component are
 * on the interval [0, 63].
 *
 * The approximation x/10 ~= (x*26) / 256 is exact for all values in this
 * interval.
 *
 * Two bytes are written to the output buffer.  The digits are padded with
 * ASCII spaces on the left.
 */
#define U6_ASCII(dst, src) do { \
	0[dst]  = (src) * 26u >> 8; \
	1[dst]  = 0x30 | ((src) - 10*0[dst]); \
	0[dst] |= 0x20 | ((0[dst]+0xf)&0x10); \
} while (0)

/*
 * Format 8-bit index value for PGM (P2) format.  This is a plain-text decimal
 * encoding using ASCII digits.  The acceptable values for each index are on
 * the interval [0, 255].
 *
 * The approximation x/10 ~= (x*205) / 2048 is exact for all values in this
 * interval.
 *
 * Three bytes are written to the output buffer.  The digits are padded with
 * ASCII spaces on the left.
 */
static void fmt_u8_ascii(char *buf, unsigned char x)
{
	unsigned char q;
	char *p = buf+1;

	buf[0] = '\40';
	if ((q = ((x*205)>>8)>>3) > 0) {
		p--[1] = 0x30|(x - 10*q);
		x = q;
	}
	U6_ASCII(p, x);
}

/*
 * Output a 16-bit value for all formats.  This is a plain-text decimal
 * encoding using ASCII digits.
 *
 * The approximation x/10 =~ ((x+1)*13107) / 131072 which is exact for the full
 * range of 16-bit values.  The approximation (x*6554) / 65536 is subsequently
 * used as this is exact for all values needing four or fewer decimal digits.
 *
 * The digits are prefixed by sep (usually a space or newline).
 */
static int print_u16_ascii(FILE *f, uint_least16_t x, char sep)
{
	char buf[8], *p = &buf[sizeof buf - 1];
	uint_least16_t q, x1;

	if ((x1 = x+1) == 0) {
		static const char max[5] = "\66\65\65\63\65";
		if (fputc(sep, f) == EOF)
			return 0;
		return fwrite(max, 5, 1, f);
	}

	q = ((x1 * (uint_least32_t)13107) >> 16) >> 1;
	while (q > 0) {
		*p-- = 0x30|(x - 10*q);
		x = q, q = (x * (uint_least32_t)6554) >> 16;
	}

	*p-- = 0x30|x;
	*p = sep;

	return fwrite(p, sizeof buf - (p-buf), 1, f);
}

/* P1/P2/P3 file header strings (ASCII encoded) */
static const char ppm_fmt_strings[] = "\
\120\61\12\0\
\120\62\12\62\65\65\12\0\
\120\63\12\66\63\12";

enum {
	PPM_FMT_P1 = 0, /* "P1\n\0" */
	PPM_FMT_P2 = 4, /* "P2\n255\n\0" */
	PPM_FMT_P3 = 12 /* "P3\n63\n\0" */
};

/*
 * Write the header for the P1, P2 or P3 formats.
 *
 * This consists of a constant string which varies on the format, followed by
 * the ASCII-encoded width and height, followed by another constant string
 * which varies by the format.
 *
 * The constant are selected by the fmt argument, which should be one of
 * PPM_FMT_P1, PPM_FMT_P2 and PPM_FMT_P3.
 */
static int write_ppm_head(FILE *f, unsigned fmt, unsigned w, unsigned h)
{
	const char *s = ppm_fmt_strings+fmt;

	if (!fwrite(s, 2, 1, f))
		return 0;
	if (!print_u16_ascii(f, w, '\12'))
		return 0;
	if (!print_u16_ascii(f, h, '\40'))
		return 0;
	if (fputs(s+2, f) == EOF)
		return 0;

	return 1;
}

static void *alloc_row(uint_least16_t width, unsigned char depth)
{
	void *ret;

	/*
	 * It is hoped that with 32-bit size_t compilers can recognize
	 * that the below size check is always true.
	 *
	 * We expand the allocation slightly in order to support writing
	 * 32-bits at a time for the 24-bit RGB PAM writer.
	 */
	if (width <= (size_t)-4 / depth)
		if ((ret = malloc((size_t)width * depth + 3)))
			return ret;

	tool_error("failed to allocate memory");
	return NULL;
}

/*
 * Output filter for Netpbm's "plain" PBM format.  This is a bitmap format
 * which is not really suitable for image data, but it can output the image
 * transparency mask, and thus complements the PPM output.  The image colour
 * data is lost in the conversion.
 */
int img_output_pbm(FILE *f, const char *filename,
                   unsigned width, unsigned height,
                   unsigned char *pixels, bitmap_slice *mask,
                   struct lbx_colour *palette)
{
	uint_least32_t offset = 0;
	unsigned x, y;
	char *rowbuf;

	if (!(rowbuf = alloc_row(width, 2)))
		return -1;

	if (!write_ppm_head(f, PPM_FMT_P1, width, height))
		goto write_err;

	memset(rowbuf, '\40', (size_t)2*width);
	if (width) rowbuf[(size_t)2*width-1] = '\12';

	for (y = 0; y < height; y++) {
		char *dst = rowbuf;

		for (x = 0; x < width; x++) {
			*dst = 0x31 ^ bitmap_test_bit(mask, offset);

			offset++;
			dst += 2;
		}

		if (fwrite(rowbuf, width, 2, f) < 2)
			goto write_err;
	}

	free(rowbuf);
	return 0;
write_err:
	tool_error("%s: write failed: %s", filename, strerror(errno));
	free(rowbuf);
	return -1;
}

/*
 * Helper for Netpbm's "plain" PGM output.  This is only meant for no-palette
 * mode as normally LBX images are not grayscale, so it is not available as a
 * normal output format.
 */
static int write_pgm(FILE *f, unsigned width, unsigned height,
                     unsigned char *pixels, bitmap_slice *mask)
{
	unsigned long offset = 0;
	unsigned x, y;
	char *rowbuf;
	int ret = -1;

	if (!(rowbuf = alloc_row(width, 4)))
		return -1;

	if (!write_ppm_head(f, PPM_FMT_P2, width, height))
		goto out;

	memset(rowbuf, '\40', (size_t)4*width);
	if (width) rowbuf[(size_t)4*width-1] = '\12';

	for (y = 0; y < height; y++) {
		char *dst = rowbuf;

		for (x = 0; x < width; x++) {
			unsigned p = pixels[offset];

			fmt_u8_ascii(dst, p & -bitmap_test_bit(mask, offset));
			offset++;
			dst += 4;
		}

		if (fwrite(rowbuf, width, 4, f) < 4)
			goto out;
	}

	ret = 0;
out:
	free(rowbuf);
	return ret;
}

static int write_ppm(FILE *f, unsigned width, unsigned height,
                     unsigned char *pixels, bitmap_slice *mask,
                     struct lbx_colour *palette)
{
	unsigned long offset = 0;
	unsigned x, y;
	char *rowbuf;
	int ret = -1;

	if (!(rowbuf = alloc_row(width, 9)))
		return -1;

	if (!write_ppm_head(f, PPM_FMT_P3, width, height))
		goto out;

	memset(rowbuf, '\40', (size_t)width*9);
	if (width) rowbuf[(size_t)width*9-1] = '\12';

	for (y = 0; y < height; y++) {
		char *dst = rowbuf;

		for (x = 0; x < width; x++) {
			struct lbx_colour c = {0,0,0};

			if (bitmap_test_bit(mask, offset))
				c = palette[pixels[offset]];

			U6_ASCII(dst,   c.red);
			U6_ASCII(dst+3, c.green);
			U6_ASCII(dst+6, c.blue);

			dst += 9;
			offset++;
		}

		if (fwrite(rowbuf, width, 9, f) < 9)
			goto out;
	}

	ret = 0;
out:
	free(rowbuf);
	return ret;
}

/*
 * Output filter for Netpbm's "plain" PPM format.  This supports RGB but not
 * transparency.  As a pure text format, it is useful for testing but is not
 * particularly efficient in terms of storage.  The image mask is lost in
 * the conversion (output pixels will be black).
 */
int img_output_ppm(FILE *f, const char *filename,
                   unsigned width, unsigned height,
                   unsigned char *pixels, bitmap_slice *mask,
                   struct lbx_colour *palette)
{
	if (!palette) {
		/*
		 * For no-palette mode, write a PGM instead which is basically
		 * the same format but has only one value per pixel.
		 */
		if (write_pgm(f, width, height, pixels, mask) < 0)
			goto err;
	} else {
		if (write_ppm(f, width, height, pixels, mask, palette) < 0)
			goto err;
	}

	return 0;
err:
	tool_error("%s: write failed: %s", filename, strerror(errno));
	return -1;
}

static void pam_format_rgba(unsigned char *dst, const unsigned char *pixels,
                            bitmap_slice *mask, unsigned width,
                            unsigned long offset, struct lbx_colour *palette)
{
	unsigned x;

	for (x = 0; x < width; x++) {
		struct lbx_colour c = palette[pixels[offset]];

		if (!bitmap_test_bit(mask, offset))
			memset(dst, 0, 4);
		else {
			if (sizeof c == 4) {
				c.active = 63;
				memcpy(dst, &c, 4);
			} else {
				dst[0] = c.red;
				dst[1] = c.green;
				dst[2] = c.blue;
				dst[3] = 63;
			}
		}
		offset++;
		dst += 4;
	}
}

static void pam_format_rgb(unsigned char *dst, const unsigned char *pixels,
                           unsigned width, unsigned long offset,
                           struct lbx_colour *palette)
{
	unsigned x;

	for (x = 0; x < width; x++) {
		struct lbx_colour c = palette[pixels[offset++]];

		if (sizeof c == 4) {
			memcpy(dst, &c, 4);
			dst += 3;
		} else {
			*dst++ = c.red;
			*dst++ = c.green;
			*dst++ = c.blue;
		}
	}
}

static void
pam_format_ga(unsigned char *dst, const unsigned char *pixels,
              const bitmap_slice *mask, unsigned width, unsigned long offset)
{
	unsigned x;

	for (x = 0; x < width; x++) {
		unsigned char v = -bitmap_test_bit(mask, offset) & 0xffu;

		dst[2ul*x+0] = v & pixels[offset];
		dst[2ul*x+1] = v;
		offset++;
	}
}

static void pam_format_g(unsigned char *row, const unsigned char *pixels,
                         unsigned width, unsigned long offset)
{
	memcpy(row, pixels+offset, width);
}

/*
 * Output filter for Netpbm's PAM format.  This format combines the features
 * of all the other Netpbm formats, supporting RGB and grayscale images with
 * or without alpha channels.  This format supports all lbximg output.
 */
int img_output_pam(FILE *f, const char *filename,
                   unsigned width, unsigned height,
                   unsigned char *pixels, bitmap_slice *mask,
                   struct lbx_colour *palette)
{
	static const char pam_strings[][8] = {
		"\120\067\012\127\111\104\124\110", /* "P7\nWIDTH"  (!term) */
		"\012\110\105\111\107\110\124\000", /* "\nHEIGHT\0"         */
		"\012\104\105\120\124\110\000\000", /* "\nDEPTH\0\0"        */
		"\012\115\101\130\126\101\114\000", /* "\nMAXVAL\0"         */

		"\012\124\125\120\114\124\131\120", /* "\nTUPLTYP"  (!term) */
		"\105\040\000\122\107\102\000\107", /* "E \0RGB\0G" (!term) */
		"\122\101\131\123\103\101\114\105", /* "RAYSCALE"   (!term) */

		"\000\137\101\114\120\110\101\012", /* "\0_ALPHA\n" (!term) */
		"\105\116\104\110\104\122\012\000"  /* "ENDHDR\n" */
	};

	/* sequence of numeric header values (width, height, depth, maxval) */
	uint_least16_t hdrvals[4];

	unsigned depth = (3>>!palette) + img_is_masked(mask, width, height);
	unsigned char *rowbuf;
	unsigned long offset;
	unsigned i;

	if (!(rowbuf = alloc_row(width, depth)))
		return -1;

	if (!fwrite(pam_strings[0], 8, 1, f))
		goto write_err;

	hdrvals[0] = width;
	hdrvals[1] = height;
	hdrvals[2] = depth;
	hdrvals[3] = palette ? 63 : 255;

	for (i = 0; i < sizeof hdrvals / sizeof hdrvals[0]; i++) {
		if (!print_u16_ascii(f, hdrvals[i], '\40'))
			goto write_err;
		if (fputs(pam_strings[i+1], f) == EOF)
			goto write_err;
	}

	/* Writes either RGB or GRAYSCALE */
	if (fputs(pam_strings[i+1] + (palette ? 3 : 7), f) == EOF)
		goto write_err;

	/* Writes either _ALPHA\nENDHDR\n or just \nENDHDR\n */
	if (fputs(pam_strings[i+3] + (depth&1 ? 7 : 1), f) == EOF)
		goto write_err;

	for (offset = i = 0; i < height; i++, offset += width) {
		switch (depth) {
		case 4:
			pam_format_rgba(rowbuf, pixels, mask, width, offset, palette);
			break;
		case 3:
			pam_format_rgb(rowbuf, pixels, width, offset, palette);
			break;
		case 2:
			pam_format_ga(rowbuf, pixels, mask, width, offset);
			break;
		case 1:
			pam_format_g(rowbuf, pixels, width, offset);
			break;
		}

		if (fwrite(rowbuf, depth, width, f) < width)
			goto write_err;
	}

	free(rowbuf);
	return 0;
write_err:
	tool_error("%s: write failed: %s", filename, strerror(errno));
	free(rowbuf);
	return -1;
}
