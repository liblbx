/*
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LBX_INTERNAL_H_
#define LBX_INTERNAL_H_

#include "lbx.h"
#include "c99types.h"
#include "misc.h"
#include "error.h"

bool lbx__default_fops(void *f, struct lbx_file_ops *out);

struct lbx_pipe_state {
	FILE *f;
	uint_least32_t offset;
};

#endif
