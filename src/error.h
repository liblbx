#ifndef LBX_ERROR_H_
#define LBX_ERROR_H_

enum {
	LBX_EOK,
	LBX_EMAGIC,
	LBX_EFORMAT,
	LBX_EINVAL,
	LBX_ENOMEM,
	LBX_EEOF,
	LBX_EMAX,

	LBX_EUBASE = 9000
};

/*
 * Create a new user error.  User errors are primarily intended to be used by
 * lbx_file_ops callback functions, but can be used for any purpose.
 *
 * Returns a positive code on success (which can be subsequently passed to
 * lbx_error_raise), or a negative value on failure.
 */
int lbx_error_new(const char *str);

/*
 * Signal an error.  The given code is recorded for later retreival by
 * lbx_error_get.  Errors are reported on a first-in, first-out basis.
 * Errors are stored in a ring buffer which can overflow, at which point
 * the oldest unretrieved error will be deleted.
 *
 * Negative codes represent system errors.  That is, the negation of some
 * (positive) errno value as returned by a library function.  If a positive
 * code does not correspond to any error, nothing is recorded.
 *
 * Returns -1 if the error code was not valid, or if an unretreived code
 * was deleted.  Otherwise, this function returns 0.
 */
int lbx_error_raise(int code);

/*
 * Retrieves an error.  The oldest reported error is removed from the buffer.
 * If msg is not NULL, a pointer to a human-readable description of the error
 * is stored in *msg.
 *
 * Returns the retrieved error code, or 0 if there were no errors.
 */
int lbx_error_get(const char **msg);

/*
 * Retrieves an error.  This function is the same as lbx_error_get, except that
 * the error code is not removed from the buffer and will be reported by a
 * subsequent call to lbx_error_get or lbx_error_peek.
 */
int lbx_error_peek(const char **msg);

/*
 * Helper function for when you only care about the message.
 */
static inline const char *lbx_errmsg(void)
{
	const char *msg;

	lbx_error_get(&msg);
	return msg;
}

#endif
