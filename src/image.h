/*
 * Copyright © 2006-2011, 2013-2014, 2021, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LBXIMG_H_
#define LBXIMG_H_

struct lbx_file_ops;

struct lbx_image {
	unsigned short width, height;
	unsigned char frames, chunk, leadin;
};

struct lbx_colour {
	unsigned char red, green, blue, active;
};

struct lbx_image *lbx_img_open(void *f, const struct lbx_file_ops *fops,
                               int (*destructor)(void *));
struct lbx_image *lbx_img_fopen(const char *file);
int     lbx_img_close(struct lbx_image *img);

int lbx_img_seek(struct lbx_image *img, unsigned frame);
long lbx_img_read_row_header(struct lbx_image *img, unsigned *x, unsigned *y);
long lbx_img_read_row_data(struct lbx_image *img, void *buf);

int lbx_img_getpalette(struct lbx_image *img, struct lbx_colour *palette);
int lbx_img_loadpalette(void *f, const struct lbx_file_ops *fops,
                        struct lbx_colour *palette);

#endif
