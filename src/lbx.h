/*
 * Copyright © 2006-2010, 2013-2014, 2021, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LBX_H_
#define LBX_H_

#include <stddef.h>

struct lbx_file_ops {
	size_t (*read)(void *buf, size_t size, void *handle);
	int    (*seek)(void *handle, long offset, int whence);
	int    (*eof) (void *handle);
};

/* I/O operations for LBX archive members. */
extern const struct lbx_file_ops lbx_arch_fops;

/* Opaque */
typedef struct lbx_file_state LBXfile;

struct lbx {
	unsigned nfiles;
};

struct lbx_statbuf {
	const char *name;
	size_t size;
};

/* Archive operations */
typedef int lbx_destructor(void *);

struct lbx *lbx_open(void *, const struct lbx_file_ops *,
                     lbx_destructor *, const char *);
struct lbx *lbx_fopen(const char *);
int lbx_close(struct lbx *);

/* File operations */
int      lbx_file_stat(struct lbx *lbx, unsigned fileno, struct lbx_statbuf *out);
LBXfile *lbx_file_open(struct lbx *lbx, unsigned fileno);
size_t   lbx_file_read(LBXfile *f, void *buf, size_t n);
int      lbx_file_seek(LBXfile *f, long offset, int whence);
long     lbx_file_tell(LBXfile *f);
int      lbx_file_eof(LBXfile *f);
void     lbx_file_close(LBXfile *f);

#endif
