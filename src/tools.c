/*
 * 2ooM: The Master of Orion II Reverse Engineering Project
 * Helper functions for liblbx command-line applications.
 *
 * Copyright © 2013, 2021, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include <assert.h>

#include "tools.h"

static const char *progname, *argv0;

void tool_init(const char *name, int argc, char **argv)
{
	progname = name;
	argv0 = argv[0] ? argv[0] : progname;
}

void tool_version(void)
{
	printf("%s (%s) %s\n", progname, PACKAGE_NAME, PACKAGE_VERSION);
	printf("Copyright (C) 2013 Nick Bowler.\n");
	puts("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.");
	puts("This is free software: you are free to change and redistribute it.");
	puts("There is NO WARRANTY, to the extent permitted by law.");
}

const char *tool_invocation(void)
{
	return argv0;
}

void tool_verror(const char *prefix, const char *fmt, va_list ap)
{
	fprintf(stderr, "%s: %s", argv0, prefix);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
}

void tool_error(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	tool_verror("error: ", fmt, ap);
	va_end(ap);
}

void tool_warn(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	tool_verror("warning: ", fmt, ap);
	va_end(ap);
}
