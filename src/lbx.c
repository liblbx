/*
 *  2ooM: The Master of Orion II Reverse Engineering Project
 *  Library for working with LBX archive files.
 *
 *  Copyright © 2006-2010, 2013-2014, 2021, 2024 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "lbx-internal.h"
#include "pack.h"

#define LBX_MAGIC    0x0000fead
#define LBX_HDR_SIZE 8

struct lbx_priv {
	struct lbx pub;

	struct lbx_io_context {
		unsigned char size;

		struct lbx_file_ops   fops_alloc;
		struct lbx_pipe_state pipe_alloc[FLEXIBLE_ARRAY_MEMBER];
	} *context;

	const struct lbx_file_ops *fops;
	int (*dtor)(void *handle);
	void *f;

	uint_least32_t offsets[FLEXIBLE_ARRAY_MEMBER];
};

struct lbx_file_state {
	struct lbx_priv *lbx;
	uint_least32_t base, limit, offset;
	int eof;
};

static const char *ctx_name(struct lbx_io_context *ctx)
{
	return (char *)ctx + *(unsigned char *)ctx;
}

static struct lbx_priv *lbx_init(unsigned char *hdr)
{
	unsigned short nfiles  = unpack_16_le(hdr+0);
	unsigned long  magic   = unpack_32_le(hdr+2);
	unsigned short version = unpack_16_le(hdr+6);
	struct lbx_priv *lbx;

	if (magic != LBX_MAGIC) {
		lbx_error_raise(LBX_EMAGIC);
		return NULL;
	}

	lbx = malloc(sizeof *lbx + sizeof lbx->offsets[0] * (nfiles+1));
	if (!lbx) {
		lbx_error_raise(LBX_ENOMEM);
		return NULL;
	}

	lbx->pub.nfiles = nfiles;
	return lbx;
}

static struct lbx_io_context *ctx_user_fops(const char *name, size_t n)
{
	unsigned char *ctx;

	if (!(ctx = malloc(n+1))) {
		lbx_error_raise(LBX_ENOMEM);
		return NULL;
	}

	*ctx = 1;
	memcpy(ctx+1, name, n);
	return (void *)ctx;
}

static struct lbx_io_context *ctx_stdio(void **f, const char *name, size_t n)
{
	struct lbx_file_ops my_fops;
	struct lbx_io_context *ctx;
	bool pipe_flag;
	unsigned size;

	size = offsetof(struct lbx_io_context, pipe_alloc);
	if ((pipe_flag = lbx__default_fops(*f, &my_fops)))
		size += sizeof *ctx->pipe_alloc;

	if (!(ctx = malloc(n + size))) {
		lbx_error_raise(LBX_ENOMEM);
		return NULL;
	}

	ctx->size = size;
	ctx->fops_alloc = my_fops;

	if (pipe_flag) {
		ctx->pipe_alloc->offset = 0;
		ctx->pipe_alloc->f = *f;
		*f = ctx->pipe_alloc;
	}

	memcpy((char *)ctx + size, name, n);
	return ctx;
}

static struct lbx_io_context *
lbx_early_init(void **f, const struct lbx_file_ops **fops, const char *name)
{
	struct lbx_io_context *ctx;
	size_t n = strlen(name)+1;

	if (*fops)
		return ctx_user_fops(name, n);

	if ((ctx = ctx_stdio(f, name, n))) {
		*fops = &ctx->fops_alloc;
	}

	return ctx;
}

struct lbx *lbx_open(void *f, const struct lbx_file_ops *fops,
                     int (*destructor)(void *), const char *name)
{
	unsigned char hdr_buf[LBX_HDR_SIZE];
	struct lbx_priv *lbx = NULL;
	struct lbx_io_context *ctx;
	unsigned i = 0;

	if (!(ctx = lbx_early_init(&f, &fops, name)))
		return NULL;

	if (fops->read(hdr_buf, sizeof hdr_buf, f) != sizeof hdr_buf) {
		if (fops->eof(f))
			lbx_error_raise(LBX_EEOF);
		goto err;
	}

	lbx = lbx_init(hdr_buf);
	if (!lbx)
		goto err;

	lbx->context = ctx;
	lbx->dtor    = destructor;
	lbx->fops    = fops;
	lbx->f       = f;

	for (i = 0; i <= lbx->pub.nfiles; i++) {
		unsigned char buf[4];

		if (fops->read(buf, sizeof buf, f) != sizeof buf) {
			if (fops->eof(f))
				lbx_error_raise(LBX_EEOF);
			goto err;
		}

		lbx->offsets[i] = unpack_32_le(buf);
	}

	return &lbx->pub;
err:
	free(lbx);
	free(ctx);
	return NULL;
}

static int file_close(void *f)
{
	return fclose((FILE *)f);
}

static char *last_component(const char *name)
{
	char *c;

	/* TODO: Handle other path separators. */
	c = strrchr(name, '/');
	if (!c)
		return (char *)name;
	return c+1;
}

struct lbx *lbx_fopen(const char *file)
{
	const char *name = last_component(file);
	FILE *f;

	if (!(f = fopen(file, "rb"))) {
		lbx_error_raise(-errno);
		return NULL;
	}

	return lbx_open(f, NULL, file_close, name);
}

int lbx_file_stat(struct lbx *pub, unsigned fileno, struct lbx_statbuf *buf)
{
	struct lbx_priv *lbx = (struct lbx_priv *)pub;
	static char str[256]; /* FIXME */

	if (fileno >= lbx->pub.nfiles) {
		lbx_error_raise(LBX_EINVAL);
		buf->name = NULL;
		return -1;
	}

	snprintf(str, sizeof str, "%s.%03u", ctx_name(lbx->context), fileno);
	buf->name = str;
	buf->size = lbx->offsets[fileno+1] - lbx->offsets[fileno];
	return 0;
}

int lbx_close(struct lbx *pub)
{
	struct lbx_priv *lbx = (struct lbx_priv *)pub;
	int rc = 0;

	if (lbx) {
		if (lbx->dtor)
			rc = lbx->dtor(lbx->f);
		free(lbx->context);
		free(lbx);
	}

	return rc;
}

struct lbx_file_state *lbx_file_open(struct lbx *pub, unsigned fileno)
{
	struct lbx_priv *lbx = (struct lbx_priv *)pub;
	struct lbx_file_state *state;

	if (fileno >= lbx->pub.nfiles) {
		lbx_error_raise(LBX_EINVAL);
		return NULL;
	}

	state = malloc(sizeof *state);
	if (!state) {
		lbx_error_raise(LBX_ENOMEM);
		return NULL;
	}

	state->base   = lbx->offsets[fileno];
	state->limit  = lbx->offsets[fileno+1] - lbx->offsets[fileno];
	state->lbx    = lbx;
	state->offset = 0;
	state->eof    = 0;

	return state;
}

size_t lbx_file_read(struct lbx_file_state *f, void *buf, size_t n)
{
	const struct lbx_file_ops *fops = f->lbx->fops;
	size_t want = MIN(n, f->limit - f->offset);
	size_t rc;

	if (fops->seek(f->lbx->f, f->base + f->offset, SEEK_SET) != 0)
		return 0;

	rc = fops->read(buf, want, f->lbx->f);
	f->offset += rc;

	if (rc < want) {
		if (fops->eof(f->lbx->f))
			lbx_error_raise(LBX_EEOF);
	} else if (rc < n) {
		f->eof = 1;
	}

	return rc;
}

int lbx_file_seek(struct lbx_file_state *f, long offset, int whence)
{
	const struct lbx_file_ops *fops = f->lbx->fops;
	unsigned long pos;

	switch (whence) {
	case SEEK_CUR:
		pos = f->offset + offset;
		break;
	case SEEK_SET:
		pos = offset;
		break;
	case SEEK_END:
		pos = f->limit + offset;
		break;
	default:
		lbx_error_raise(LBX_EINVAL);
		return -1;
	}

	if (pos > f->limit) {
		lbx_error_raise(LBX_EINVAL);
		return -1;
	}

	f->offset = pos;
	f->eof = 0;

	return 0;
}

long lbx_file_tell(struct lbx_file_state *f)
{
	return f->offset;
}

int lbx_file_eof(struct lbx_file_state *f)
{
	return f->eof;
}

void lbx_file_close(struct lbx_file_state *f)
{
	free(f);
}
