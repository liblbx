/*
 *  2ooM: The Master of Orion II Reverse Engineering Project
 *  Simple command-line tool to extract LBX archive files.
 *
 *  Copyright © 2006-2011, 2013-2014, 2021, 2024 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "help.h"
#include "gnu_getopt.h"

#include "tools.h"
#include "lbx.h"
#include "error.h"
#include "glob.h"

#include "toolopts.h"
#include "c99types.h"

static const char sopts[] = SOPT_STRING;
static const struct option lopts[] = { LOPTS_INITIALIZER, {0} };

static void print_usage(FILE *f)
{
	const char *progname = tool_invocation();

	fprintf(f, "Usage: %s [options] [-l|-x] [file ...]\n", progname);
	if (f != stdout)
		fprintf(f, "Try %s --help for more information.\n", progname);
}

static void print_help(void)
{
	const struct option *opt;

	print_usage(stdout);

	puts("This is \"lbxtool\": a command-line tool for manipulating the LBX archives\n"
	     "used in Moo2.");

	putchar('\n');
	puts("Options:");
	for (opt = lopts; opt->name; opt++) {
		struct lopt_help help;
		int w;

		if (!lopt_get_help(opt, &help))
			continue;

		help_print_option(opt, help.arg, help.desc, 20);
	}
	putchar('\n');

	puts("For more information, see the lbxtool(1) man page.");
	putchar('\n');

	printf("Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
}

enum {
	MODE_NONE,
	MODE_LIST,
	MODE_EXTRACT
};

static int compile_globs(char **argv)
{
	char **out = argv;
	unsigned i;

	for (i = 0; argv[i]; i++) {
		struct glob_nfa *p;

		if (!(p = glob_compile(argv[i]))) {
			tool_error("cannot compile glob: %s", argv[i]);
		} else {
			*out++ = (char *)p;
		}
	}

	*out = NULL;
	return argv[0] != NULL;
}

static int match_globs(char **argv, const char *name)
{
	while (*argv) {
		if (glob_match((struct glob_nfa *)*argv++, name))
			return 1;
	}

	return 0;
}

static int list(struct lbx *lbx, int verbose, char **argv)
{
	unsigned int i;
	int use_globs;

	if (verbose) {
		printf("Files in archive: %u\n", lbx->nfiles);
	}

	use_globs = compile_globs(argv);
	for (i = 0; i < lbx->nfiles; i++) {
		struct lbx_statbuf stat;

		lbx_file_stat(lbx, i, &stat);
		if (use_globs && !match_globs(argv, stat.name))
			continue;

		printf("%s", stat.name);
		if (verbose) {
			printf(" size=%zu bytes", stat.size);
		}

		putchar('\n');
	}

	return EXIT_SUCCESS;
}

static int extract_file(const char *dst, LBXfile *f)
{
	unsigned char buf[1024];
	size_t len;
	FILE *of;

	if (!(of = fopen(dst, "wb")))
		goto output_err;

	do {
		len = lbx_file_read(f, buf, sizeof buf);
		if (len > 0 && !fwrite(buf, len, 1, of)) {
			goto output_err;
		}
	} while (len == sizeof buf);

	if (fclose(of) == 0)
		return lbx_file_eof(f);
	of = NULL;
output_err:
	tool_error("%s: write failed: %s", dst, strerror(errno));
	if (of) fclose(of);
	return -1;
}

static int extract(struct lbx *lbx, int verbose, char **argv)
{
	bool failed = 0;
	unsigned int i;
	int use_globs;

	if (verbose) {
		printf("Files in archive: %u\n", lbx->nfiles);
	}

	use_globs = compile_globs(argv);
	for (i = 0; i < lbx->nfiles; i++) {
		struct lbx_statbuf stat;
		const char *dst;
		LBXfile *file;
		int rc;

		lbx_file_stat(lbx, i, &stat);
		dst = stat.name;

		if (use_globs && !match_globs(argv, dst))
			continue;

		file = lbx_file_open(lbx, i);
		if (!file || !(rc = extract_file(dst, file)))
			tool_error("%s: read failed: %s", dst, lbx_errmsg());

		lbx_file_close(file);

		if (verbose && rc == 1) printf("wrote %s\n", dst);

		failed |= (rc != 1);
	}

	return failed ? EXIT_FAILURE : EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
	int mode = MODE_NONE, verbose = 0, opt, rc = EXIT_FAILURE;
	const char *file = NULL;
	struct lbx *lbx;

	tool_init("lbxtool", argc, argv);
	while ((opt = getopt_long(argc, argv, sopts, lopts, NULL)) != -1) {
		switch(opt) {
		case 'l':
			mode = MODE_LIST;
			break;
		case 'x':
			mode = MODE_EXTRACT;
			break;
		case 'f':
			file = optarg;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'V':
			tool_version();
			return EXIT_SUCCESS;
		case 'H':
			print_help();
			return EXIT_SUCCESS;
		default:
			print_usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if (file) {
		lbx = lbx_fopen(file);
	} else {
		lbx = lbx_open(stdin, NULL, NULL, "stdin");
		file = "standard input";
	}

	if (!lbx) {
		tool_error("%s: read failed: %s", file, lbx_errmsg());
		return EXIT_FAILURE;
	}

	switch (mode) {
	case MODE_LIST:
		rc = list(lbx, verbose, &argv[optind]);
		break;
	case MODE_EXTRACT:
		rc = extract(lbx, verbose, &argv[optind]);
		break;
	default:
		tool_error("no mode specified");
		print_usage(stderr);
	}

	lbx_close(lbx);
	return rc;
}
