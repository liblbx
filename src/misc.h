#ifndef LBX_MISC_H_
#define LBX_MISC_H_

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#ifndef NDEBUG
#  define lbx_assert(expr) ((void)((expr) ? 1 : \
	fprintf(stderr, "format assertion failed: %s [%s:%d]\n", \
	                  #expr, __FILE__, __LINE__)))
#else
#  define lbx_assert(expr) ((void)(expr))
#endif

#endif
