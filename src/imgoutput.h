/*
 * 2ooM: The Master of Orion II Reverse Engineering Project
 * Image output definitions for lbximg.
 *
 * Copyright © 2013-2014, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LBX_IMGOUTPUT_H_
#define LBX_IMGOUTPUT_H_

#include "image.h"
#include "c99types.h"

typedef int img_output_func(FILE *f, const char *filename,
                            unsigned width, unsigned height,
                            unsigned char *pixels, bitmap_slice *mask,
                            struct lbx_colour *palette);

img_output_func img_output_pbm, img_output_ppm, img_output_pam;
img_output_func img_output_png;

bool img_is_masked(bitmap_slice *mask, unsigned width, unsigned height);

#endif
