/*
 *  2ooM: The Master of Orion II Reverse Engineering Project
 *  Simple command-line tool to convert an LBX image to other formats.
 *
 *  Copyright © 2006-2011, 2013-2014, 2021, 2024 Nick Bowler
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>

#include "help.h"
#include "xtra.h"
#include "gnu_getopt.h"

#include "tools.h"
#include "image.h"
#include "error.h"
#include "lbx.h"
#include "imgoutput.h"
#include "imgopts.h"

#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* Global options */
static bool no_palette, have_palette, verbose;
static signed char format_id;

static const char *outname = "out_";
static const char *infile = NULL;

static struct lbx_colour lbx_palette[256];

static void print_usage(FILE *f)
{
	const char *progname = tool_invocation();

	fprintf(f, "Usage: %s [options] [-i|-d] [frame ...]\n", progname);
	if (f != stdout)
		fprintf(f, "Try %s --help for more information.\n", progname);
}

static void print_help(const struct option *lopts)
{
	const struct option *opt;
	struct lopt_help help;

	print_usage(stdout);

	putchar('\n');
	puts("Options:");
	for (opt = lopts; opt->name; opt++) {
		if (!lopt_get_help(opt, &help))
			continue;

		help_print_option(opt, help.arg, help.desc, 20);
	}
	putchar('\n');

	puts("For more information, see the lbximg(1) man page.");
	putchar('\n');

	printf("Report bugs to <%s>.\n", PACKAGE_BUGREPORT);
}

enum {
	FMT_PAM,
	FMT_PBM,
	FMT_PNG,
	FMT_PPM
};

static const char format_ext[][4] = {
	"pam",
	"pbm",
	"png",
	"ppm"
};

static int lookup_format(const char *fmt)
{
	unsigned i;

	if (!fmt)
		return FMT_PAM;

	for (i = 0; i < sizeof format_ext / sizeof format_ext[0]; i++) {
		if (!strcmp(fmt, format_ext[i]))
			goto found;
	}

	tool_error("unknown format %s", fmt);
	return -1;
found:
#if !HAVE_LIBPNG
	if (i == FMT_PNG) {
		tool_error("%s support unavailable", fmt);
		return -1;
	}
#endif
	return i;
}

bool img_is_masked(bitmap_slice *mask, unsigned width, unsigned height)
{
	uint_least32_t sz = (uint_least32_t) width * height;
	long i, n = BITMAP_SIZE(sz);

	for (i = 0; i < n-1; i++) {
		if (mask[i] != BITMAP_MAX)
			return true;
	}

	if (mask[i] != (BITMAP_MAX >> (-sz & BITMAP_MASK)))
		return true;

	return false;
}

static int output(const char *name, int fmt, unsigned char *pixels,
                  bitmap_slice *mask, struct lbx_image *img)
{
	struct lbx_colour *palette = no_palette ? NULL : lbx_palette;
	img_output_func *fmt_output;
	FILE *of;
	int rc;

	switch (fmt) {
	case FMT_PAM: fmt_output = img_output_pam; break;
	case FMT_PBM: fmt_output = img_output_pbm; break;
	case FMT_PPM: fmt_output = img_output_ppm; break;
#if HAVE_LIBPNG
	case FMT_PNG: fmt_output = img_output_png; break;
#endif
	default: assert(0);
	}

	if (!(of = fopen(name, "wb"))) {
		tool_error("%s: failed to open: %s", name, strerror(errno));
		return -1;
	}

	rc = fmt_output(of, name, img->width, img->height,
	                pixels, mask, palette);
	if (rc < 0) {
		fclose(of);
		return -1;
	}

	if (fclose(of) == EOF) {
		tool_error("%s: write failed: %s", name, strerror(errno));
		return -1;
	}

	if (verbose)
		printf("wrote %s\n", name);

	return 0;
}

/* Return true iff a divides b. */
static bool divides(unsigned a, unsigned b)
{
	if (b == 0)
		return true;
	if (a == 0)
		return false;

	return b % a == 0;
}

static int decode_frame(struct lbx_image *img, unsigned n,
                        unsigned char *pixels, bitmap_slice *mask)
{
	unsigned x, y;
	long rc;

	rc = lbx_img_seek(img, n);
	if (rc < 0) {
		tool_error("frame %u: invalid frame: %s", n, lbx_errmsg());
		return -1;
	}

	while ((rc = lbx_img_read_row_header(img, &x, &y)) != 0) {
		unsigned long offset;

		if (rc < 0) {
			tool_error("frame %u: invalid row: %s", n, lbx_errmsg());
			return -1;
		}

		offset = (unsigned long) y * img->width + x;
		rc = lbx_img_read_row_data(img, pixels+offset);
		if (rc < 0) {
			tool_error("frame %u: error reading row: %s", n, lbx_errmsg());
			return -1;
		}

		if (rc)
			bitmap_set_range(mask, offset, offset+rc-1);
	}

	return 0;
}

static int parse_range(struct bitmap256 *dst, char *spec, unsigned nframes)
{
	unsigned char range[3] = { 0 };
	unsigned state = 0, cur = 0;
	unsigned char c;
	char *s;

	range[1] = nframes-1;
	for (s = spec; (c = *s); s++) {
		if (state == 0 && c == '-') {
			state++;
			cur = 0;
		} else if ((c -= '0') < 10 && (cur = 10 * cur + c) < 256) {
			range[state] = cur;
		} else {
			goto invalid;
		}
	}

	if ((range[state+1] = range[state]) < range[0])
		goto invalid;

	if (range[state] >= nframes)
		tool_warn("%s: frame out of range", spec);

	bitmap256_set_range(dst, range[0], range[1]);
	return 0;
invalid:
	tool_error("%s: invalid frame specification", spec);
	return -1;
}

static int parse_ranges(struct bitmap256 *dst, char **argv, struct lbx_image *img)
{
	unsigned i;
	int rc;

	for (i = 0; i < BITMAP256_COUNT; i++)
		dst->map[i] = argv[0] ? 0 : -1;

	for (i = 0; argv[i]; i++) {
		if ((rc = parse_range(dst, argv[i], img->frames)) < 0)
			return -1;
	}

	return 0;
}

static bitmap_slice *alloc_scratch(struct lbx_image *img, uint_least32_t *sz)
{
	uint_least32_t img_sz, mask_sz;
	bitmap_slice *ret;

	img_sz = (uint_least32_t) img->width * img->height;
	if (!img_sz) {
		img_sz = 1;
		*sz = mask_sz = 0;
	} else {
		*sz = mask_sz = BITMAP_SIZE(img_sz) * sizeof (bitmap_slice);
	}

	if (img_sz <= (size_t)-((size_t)1 + mask_sz)) {
		if ((ret = calloc((size_t) img_sz + mask_sz, 1))) {
			return ret;
		}
	}

	tool_error("failed to allocate memory");
	return NULL;
}

/* Format n into three decimal digits, writing to dst. */
static void update_name(char *dst, unsigned char n)
{
	unsigned char q;

	q = ((n*205)>>8)>>3;
	dst[2]  = '0' + (n - 10*q);
	dst[0]  = (q*26)>>8;
	dst[1]  = '0' + (q - 10*dst[0]);
	dst[0] += '0';
}

static int decode(struct lbx_image *img, int fmt, char **argv)
{
	struct bitmap256 selected;
	uint_least32_t mask_sz;
	unsigned char *pixels;
	bitmap_slice *mask;
	char *name, *ext;
	size_t name_sz;

	bool extracted = false;
	int i, ret;

	if (!no_palette && !have_palette && fmt != FMT_PBM)
		tool_warn("empty palette");

	if (parse_ranges(&selected, argv, img))
		return EXIT_FAILURE;

	if (!(mask = alloc_scratch(img, &mask_sz)))
		return EXIT_FAILURE;
	pixels = (unsigned char *)mask + mask_sz;

	name_sz = strlen(outname);
	if (!(name = malloc(name_sz + sizeof ".000.xyz")))
		return free(mask), EXIT_FAILURE;

	ext = (char *)memcpy(name, outname, name_sz) + name_sz;
	memcpy(ext+4, format_ext[fmt], 4);
	ext[3] = '.';

	/* Extract the images, in order. */
	ret = EXIT_SUCCESS;
	for (i = 0; i < img->frames; i++) {
		if (divides(img->chunk, i))
			memset(mask, 0, mask_sz);

		if (decode_frame(img, i, pixels, mask) < 0) {
			ret = EXIT_FAILURE;
			break;
		}

		if (!bitmap256_test_bit(&selected, i))
			continue;

		update_name(ext, i);
		if (output(name, fmt, pixels, mask, img) < 0)
			ret = EXIT_FAILURE;
		else
			extracted = true;
	}

	if (!extracted)
		tool_warn("no frames extracted");

	free(name);
	free(mask);
	return ret;
}

static int load_base_palette(const char *file, struct lbx_colour *out)
{
	FILE *f;
	int n;

	if (!(f = fopen(file, "rb"))) {
		tool_error("%s: failed to open: %s", file, strerror(errno));
		return -1;
	}

	if ((n = lbx_img_loadpalette(f, NULL, out)) < 0)
		tool_error("%s: read failed: %s", file, lbx_errmsg());

	fclose(f);
	return n;
}

static int load_over_palette(const char *file, struct lbx_colour *out)
{
	struct lbx_image *img;
	int n;

	if (!(img = lbx_img_fopen(file))) {
		tool_error("%s: failed to open: %s", file, lbx_errmsg());
		return -1;
	}

	if ((n = lbx_img_getpalette(img, out)) < 0)
		tool_error("%s: read failed: %s", file, lbx_errmsg());
	else if (!n)
		tool_warn("%s: no embedded palette", file);

	lbx_img_close(img);
	return n;
}

static int open_image(const char **filename, struct lbx_colour *palette,
                                             struct lbx_image **out_img)
{
	const char *f = *filename;
	struct lbx_image *img;
	int n;

	if (!f || !strcmp(f, "-")) {
		if (!(img = lbx_img_open(stdin, NULL, NULL)))
			goto err_open;
		*filename = "standard input";
	} else if (!(img = lbx_img_fopen(f))) {
		goto err_open;
	}

	if ((n = lbx_img_getpalette(img, palette)) < 0) {
		tool_error("%s: read failed: %s", infile, lbx_errmsg());
		lbx_img_close(img);
		return -1;
	}

	*out_img = img;
	return n;
err_open:
	tool_error("%s: failed to open: %s", infile, lbx_errmsg());
	return -1;
}

enum {
	MODE_EXIT_SUCCESS = -1,
	MODE_EXIT_FAILURE =  0,
	MODE_NONE = MODE_EXIT_FAILURE,
	MODE_DECODE,
	MODE_IDENT
};

static void identify(const char *filename, int palcount, struct lbx_image *img)
{
	printf("%s: %ux%u LBX image", filename, (unsigned)img->width,
	                                        (unsigned)img->height);
	if (palcount)
		printf(", %d palette entries", palcount);
	if (img->frames > 1)
		printf(", %u frames", (unsigned)img->frames);
	if (img->chunk)
		printf(", chunked");
	if (img->leadin+1 < img->frames)
		printf(", loops");

	putchar('\n');
}

static int initialize(int argc, char **argv, struct lbx_image **out_img)
{
	struct lbx_colour tmp_palette[256], pink = { 63, 0, 63, 0 };
	int mode = MODE_NONE;
	int i, rc, opt;

	XTRA_PACKED_LOPTS2(lopts, packed_lopts);
	tool_init("lbximg", argc, argv);

	for (i = 0; i < 256; i++)
		tmp_palette[i] = pink;

	while ((opt = getopt_long(argc, argv, SOPT_STRING, lopts, 0)) != -1) {
		switch (opt) {
		case 'i':
			mode = MODE_IDENT;
			break;
		case 'd':
			mode = MODE_DECODE;
			break;
		case 'v':
			verbose = true;
			break;
		case 'F':
			if ((format_id = lookup_format(optarg)) < 0)
				return MODE_EXIT_FAILURE;
			break;
		case 'f':
			infile = optarg;
			break;
		case 'n':
			no_palette = true;
			break;
		case 'p':
			if (load_base_palette(optarg, tmp_palette) < 0)
				return MODE_EXIT_FAILURE;
			have_palette = true;
			break;
		case 'O':
			if ((rc = load_over_palette(optarg, lbx_palette)) < 0)
				return MODE_EXIT_FAILURE;
			else if (rc)
				have_palette = true;
			break;
		case LOPT_OUTPUT_PREFIX:
			outname = optarg;
			break;
		case 'V':
			tool_version();
			return MODE_EXIT_SUCCESS;
		case 'H':
			print_help(lopts);
			return MODE_EXIT_SUCCESS;
		default:
			print_usage(stderr);
			return MODE_EXIT_FAILURE;
		}
	}

	if (mode == MODE_NONE) {
		tool_error("no mode specified");
		print_usage(stderr);
	}

	if ((rc = open_image(&infile, tmp_palette, out_img)) < 0)
		return MODE_EXIT_FAILURE;
	else if (rc)
		have_palette = true;

	if (verbose || mode == MODE_IDENT)
		identify(infile, rc, *out_img);

	for (i = 0; i < 256; i++) {
		if (!lbx_palette[i].active)
			lbx_palette[i] = tmp_palette[i];
	}

	return mode;
}

int main(int argc, char **argv)
{
	struct lbx_image *img;
	int ret = 0;

	switch (initialize(argc, argv, &img)) {
	case MODE_EXIT_SUCCESS: return EXIT_SUCCESS;
	case MODE_EXIT_FAILURE: return EXIT_FAILURE;
	case MODE_DECODE:
		ret = decode(img, format_id, &argv[optind]);
	}

	lbx_img_close(img);
	return ret;
}
