/*
 * 2ooM: The Master of Orion II Reverse Engineering Project
 * Helper functions for liblbx command-line applications.
 *
 * Copyright © 2013, 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LBX_TOOLS_H_
#define LBX_TOOLS_H_

#include <stdarg.h>
#include <limits.h>
#include "c99types.h"

void tool_init(const char *name, int argc, char **argv);
void tool_version(void);

const char *tool_invocation(void);
void tool_verror(const char *prefix, const char *fmt, va_list ap);
void tool_error(const char *fmt, ...);
void tool_warn(const char *fmt, ...);

/*
 * BITMAP_SHIFT expands to the greater of 5 or 6, such that 2**BITMAP_SHIFT
 * distinct bits can be stored in an object of type unsigned long.
 */
typedef unsigned long bitmap_slice;
#if !(((ULONG_MAX >> 16) >> 16) >> 31)
/* "unsigned long" is less than 64 bits */
#  define BITMAP_SHIFT 5
#else
#  define BITMAP_SHIFT 6
#endif

/* Expands to the value with BITMAP_SHIFT least significant bits set. */
#define BITMAP_MASK ((1u << BITMAP_SHIFT) - 1)

/* Expands to the value with the 2**BITMAP_SHIFT least significant bits set. */
#define BITMAP_MAX (((bitmap_slice)2 << BITMAP_MASK) - 1)

/* Expands to the number of bitmap_slice elements required to store n bits. */
#define BITMAP_SIZE(n) (((n) + BITMAP_MASK) >> BITMAP_SHIFT)

/*
 * Expands to the index of the element containing bit n in an array of
 * bitmap_slice elements.
 */
#define BITMAP_IDX(n) ((n) >> BITMAP_SHIFT)

/*
 * Expands a single-bit mask selecting the given bit for the appropriate
 * element within an array of bitmap_slice elements.
 */
#define BITMAP_BITMASK(n) ((bitmap_slice)1 << ((n) & BITMAP_MASK))

/*
 * Set all bits on the closed interval [lo,hi] in a bitmap.
 */
static inline void
bitmap_set_range(bitmap_slice *map, uint_least32_t lo, uint_least32_t hi)
{
	bitmap_slice mask = BITMAP_MAX & (BITMAP_MAX << (lo & BITMAP_MASK));
	uint_least32_t i, hi_idx = BITMAP_IDX(hi);

	for (i = BITMAP_IDX(lo); i <= hi_idx; i++) {
		if (i == hi_idx)
			mask &= BITMAP_MAX >> ((~hi) & BITMAP_MASK);

		map[i] |= mask;
		mask = BITMAP_MAX;
	}
}

static inline bool bitmap_test_bit(const bitmap_slice *map, uint_least32_t n)
{
	return (map[BITMAP_IDX(n)] >> (n & BITMAP_MASK)) & 1;
}

/*
 * Helpers for a 256-bit bitmap.
 */
#define BITMAP256_COUNT (256 >> BITMAP_SHIFT)
struct bitmap256 {
	bitmap_slice map[BITMAP256_COUNT];
};
#define bitmap256_set_range(bm, lo, hi) bitmap_set_range((bm)->map, lo, hi)
#define bitmap256_test_bit(bm, n) bitmap_test_bit((bm)->map, n)

#endif
