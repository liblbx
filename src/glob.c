/*
 * Simple shell-like filename glob matcher.
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>

#if CHAR_BIT != 8
#  error this implementation assumes 8-bit bytes
#endif

#include "tools.h"

#define STATE_FLAG_LOOP 1u

/*
 * Define a nondeterministic transition for a particular state.
 *
 * The transitions are:
 *
 *    - on a matching input, transition to the next state.
 *    - if the loop flag is set, on any input, stay in the same state.
 *    - on any input, transition to the dead state.
 */
struct glob_transition {
	struct bitmap256 map;
	unsigned short flags;
};

/*
 * Reasonably compact representation of a nondeterministic finite automaton
 * representing a shell-like pattern matching sequences of 8-bit bytes.
 *
 * Shell patterns are fairly restrictive.  It is sufficient to have a
 * single accepting state and the transitions above are sufficient for
 * every possible state.
 *
 * States are numbered starting from 0 (the initial state), with the
 * corresponding element of delta describing the transitions.  The
 * accepting state is always the highest numbered state.  There is
 * a dead state (with no outward transitions) which is not encoded.
 */
struct glob_nfa {
	unsigned char accepting;

	struct glob_transition delta[FLEXIBLE_ARRAY_MEMBER];
};

static void bitmap256_set_bit(struct bitmap256 *map, unsigned char n)
{
	map->map[BITMAP_IDX(n)] |= BITMAP_BITMASK(n);
}

static void bitmap256_invert(struct bitmap256 *map)
{
	unsigned i;

	for (i = 0; i < BITMAP256_COUNT; i++) {
		map->map[i] = ~map->map[i];
	}
}

/*
 * Sets the given bit n in the bitmap and clears all lower bits.
 */
static void bitmap256_set_minimum(struct bitmap256 *map, unsigned char n)
{
	unsigned i, lo_idx = BITMAP_IDX(n);

	for (i = 0; i < lo_idx; i++) {
		map->map[i] = 0;
	}

	map->map[i] |= BITMAP_BITMASK(n);
	map->map[i] &= ~(BITMAP_BITMASK(n)-1);
}

/*
 * Returns a non-zero value if any bit is set in map.
 */
static bitmap_slice bitmap256_any(struct bitmap256 *map)
{
	bitmap_slice ret = 0;
	unsigned i;

	for (i = 0; i < BITMAP256_COUNT; i++) {
		ret |= map->map[i];
	}

	return ret;
}

/*
 * Allocate glob NFA with the given number of states (minimum 1).  The
 * automaton initially accepts only the empty string.
 */
static struct glob_nfa *glob_alloc(unsigned num_states)
{
	return calloc(1, offsetof(struct glob_nfa, delta)
	               + num_states * sizeof (struct glob_transition));
}

/*
 * Given a glob NFA which decides some set of words L, transform it to one
 * which decides the new set representing words prefixed by something in L:
 *
 *   L' = { lc | l in L , c in S* }
 *
 * (where S* is the set of all words).
 */
static void glob_append_star(struct glob_nfa *fsm)
{
	fsm->delta[fsm->accepting].flags |= STATE_FLAG_LOOP;
}

/*
 * Given a glob NFA which decides some set of words L, and a character class C
 * which describes a subset of the input alphabet, transform the NFA into one
 * which decides the new set representing some word in L concatenated with
 * some symbol in C:
 *
 *   L' = { lc | l in L , c in C }
 */
static void glob_append_class(struct glob_nfa *fsm, const struct bitmap256 *class)
{
	struct glob_transition *t = &fsm->delta[fsm->accepting++];

	t->map = *class;
}

/*
 * Parse the "meat" of a shell character class (that is, everything after
 * the opening bracket) such as [abc0-9].
 *
 * The class is converted to a bitmap with the bits corresponding to matching
 * characters set, and other bits clear.  Returns the number of characters
 * parsed.
 *
 * Notes on shell syntax:
 *
 *  - If the very first character is a bang ('!'), then the class is inverted.
 *
 *  - If the input begins with a hyphen ('-', possibly preceded by bang) then
 *    this is interpreted as a literal hyphen as opposed to its usual syntactic
 *    meaning as part of a character range.
 *
 *  - If the input begins with a closing bracket (']', possibly preceded by bang
 *    and/or hyphen), then this is interpreted as a literal bracket as opposed
 *    to its usual syntactic meaning as the end of the character class.
 *
 *  - Within a character range, if the final character is a hyphen or a bracket
 *    then this is interpreted literally as part of the character range.
 */
static int parse_class(const char *in, struct bitmap256 *out)
{
	const char *s = in;
	unsigned char c;
	int invert;

	memset(out, 0, sizeof *out);

	if ((invert = (*s == '!')))
		s++;
	if (*s == '-')
		bitmap256_set_bit(out, *s++);
	if (*s == ']')
		bitmap256_set_bit(out, *s++);

	for (; (c = *s) && c != ']'; s++) {
		if (c == '-' && (c = *++s))
			bitmap256_set_range(out, s[-2], c);
		else
			bitmap256_set_bit(out, c);
	}

	if (invert)
		bitmap256_invert(out);

	return s - in + !!c;
}

/*
 * Return an estimate of the number of states required to parse s.
 *
 * This estimate may be more than the true number of states but
 * generally should not be less (if it is, then the pattern will
 * fail to compile).  The result is clamped to 256 as this is the
 * maximum number of states supported.
 */
static unsigned estimate_states(const char *s)
{
	unsigned total = 1;

	while (*s) {
		size_t c;

		if ((c = strcspn(s, "[")) >= 256 - total)
			return 256;

		total += c;
		if (*(s += c)) {
			if (total++ >= 256)
				return 256;

			s++;
			if (*s == '!')
				s++;
			if (*s == '-')
				s++;
			if (*s == ']')
				s++;
			if (*(s += strcspn(s, "]")))
				s++;
		}
	}

	return total;
}

struct glob_nfa *glob_compile(const char *s)
{
	unsigned estimate = estimate_states(s);
	struct glob_nfa *fsm;
	unsigned char c;

	if (!(fsm = glob_alloc(estimate)))
		return NULL;

	for (; (c = *s); s++) {
		struct bitmap256 class;

		switch (c) {
		case '*':
			glob_append_star(fsm);
			continue;
		case '?':
			memset(&class, -1, sizeof class);
			break;
		case '[':
			s += parse_class(s+1, &class);
			break;
		default:
			memset(&class, 0, sizeof class);
			bitmap256_set_bit(&class, c);
		}

		if (--estimate == 0) {
			/* too many states */
			free(fsm);
			return NULL;
		}

		glob_append_class(fsm, &class);
	}

	return fsm;
}

/*
 * We can use a perfect hashing technique to compute the base-2 logarithm of
 * an exact power of two, as described in Leierson et al. "Using de Bruijn
 * Sequences to Index a 1 in a Computer Word".
 *
 * The constants are defined such that each BITMAP_SHIFT-bit subsequence occurs
 * exactly once.  Thus, multiplication by an exact power of two shifts one of
 * these unique subsequences into the upper bit positions (of the lower half
 * of the product).  Then we use that to index a precomputed lookup table.
 */
#if BITMAP_SHIFT == 6
#define DEBRUIJN_IDX(x) ((((x)*0x218a392cd3d5dbf) & 0xffffffffffffffff) >> 58)
#define DEBRUIJN_LUT \
	 0,  1,  2,  7,  3, 13,  8, 19,  4, 25, 14, 28,  9, 34, 20, 40, \
	 5, 17, 26, 38, 15, 46, 29, 48, 10, 31, 35, 54, 21, 50, 41, 57, \
	63,  6, 12, 18, 24, 27, 33, 39, 16, 37, 45, 47, 30, 53, 49, 56, \
	62, 11, 23, 32, 36, 44, 52, 55, 61, 22, 43, 51, 60, 42, 59, 58
#elif BITMAP_SHIFT == 5
#define DEBRUIJN_IDX(x) ((((x)*0x4653adf & 0xffffffff)) >> 27)
#define DEBRUIJN_LUT \
	 0,  1,  2,  6,  3, 11,  7, 16,  4, 14, 12, 21,  8, 23, 17, 26, \
	31,  5, 10, 15, 13, 20, 22, 25, 30,  9, 19, 24, 29, 18, 28, 27
#endif

/*
 * Iterate over each set bit in cur_state, then evaluate all associated
 * transition functions with the given character c to determine next_state.
 *
 * The original value of cur_state is clobbered.
 *
 * It's wild but gcc-12 can actually identify the below as a "count trailing
 * zeroes" operation and optimize it as such.
 */
static int step_nfa(const struct glob_nfa *fsm, struct bitmap256 *cur_state,
                    struct bitmap256 *next_state, unsigned char c)
{
	int loop_state = -1;
	int i, j;

	for (i = 0;;) {
		static const unsigned char bitpos[] = { DEBRUIJN_LUT };
		const struct glob_transition *t;
		bitmap_slice v;

		for (; i < BITMAP256_COUNT; i++) {
			if ((v = cur_state->map[i]))
				break;
		}

		if (!v) break;

		cur_state->map[i] ^= (v &= -v);
		j = (i << BITMAP_SHIFT) + bitpos[DEBRUIJN_IDX(v)];
		t = &fsm->delta[j];

		if (bitmap256_test_bit(&t->map, c)) {
			bitmap256_set_bit(next_state, ++j);

			if (j > loop_state && t[1].flags & STATE_FLAG_LOOP)
				loop_state = j;
		}
	}

	return loop_state;
}

int glob_match(const struct glob_nfa *fsm, const char *s)
{
	int loop_state = -!(fsm->delta->flags & STATE_FLAG_LOOP);
	struct bitmap256 cur_state = {1};
	unsigned char c;

	for (; (c = *s); s++) {
		struct bitmap256 next_state = {0};
		int rc;

		/*
		 * Short circuit if the loop state is an accepting one, as
		 * all remaining text will match.
		 */
		if (fsm->accepting == loop_state)
			return 1;

		/*
		 * Short circuit if we are in the dead state, as no remaining
		 * text can match.
		 */
		if (!bitmap256_any(&cur_state))
			return 0;

		rc = step_nfa(fsm, &cur_state, &next_state, c);
		if (rc > loop_state)
			loop_state = rc;

		/*
		 * Short circuit by exiting any prior loop states, as they
		 * will not meaningfully affect the result.
		 */
		if (loop_state >= 0) {
			bitmap256_set_minimum(&next_state, loop_state);
		}

		cur_state = next_state;
	}

	return bitmap256_test_bit(&cur_state, fsm->accepting);
}
