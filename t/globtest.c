/*
 * Helper to verify the filename globbing routines.
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include "help.h"
#include "gnu_getopt.h"
#include "glob.h"

static const char *progname = "globtest";
static const char sopts[] = "f:VH";
static const struct option lopts[] = {
	{ "file", 1, NULL, 'f' },
	{ "version", 0, NULL, 'V' },
	{ "help", 0, NULL, 'H' },
	{ 0 }
};

static void print_version(void)
{
	puts("\
globtest (" PACKAGE_NAME ") " PACKAGE_VERSION "\n\
Copyright (C) 2024 Nick Bowler.\n\
License GPLv3+: GNU General Public License version 3 or any later version.\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.");
}

static void print_usage(FILE *f)
{
	fprintf(f, "Usage: %s pattern [pattern ...]\n", progname);
}

static void print_help(void)
{
	const struct option *opt;

	print_usage(stdout);
	puts("Test glob matching.\n");
	puts("Options:");
	for (opt = lopts; opt->val; opt++) {
		if (help_print_optstring(opt, "ARG", 20))
			putchar('\n');
	}
}

static int do_getline(char **linebuf, size_t *n)
{
        char *work = *linebuf;
        size_t pos = 0;
        size_t sz;

        if (!work) {
                sz = 2;
                goto initial_alloc;
        }

        for (sz = *n;;) {
                if (!fgets(&work[pos], sz - pos, stdin)) {
                        if (ferror(stdin)) {
				perror(progname);
                                return -1;
                        }

                        return !!pos;
                }

                pos += strlen(&work[pos]);
                if (work[pos-1] == '\n') {
                        work[pos-1] = '\0';
                        return 1;
                }

                if (sz > INT_MAX/2 || sz > ((size_t)-1)/4)
                        break;

                sz = ((sz*4) + 2) / 3;
initial_alloc:
                work = realloc(work, sz);
                if (!work)
                        break;
                *linebuf = work;
                *n = sz;
        }

	fprintf(stderr, "%s: failed to allocate memory\n", progname);
        return -1;
}

int main(int argc, char **argv)
{
	const char *file = NULL;
	char *line = NULL;
	int opt, rc, i;
	size_t sz;

	if (argc > 0)
		progname = argv[0];

	while ((opt = getopt_long(argc, argv, sopts, lopts, NULL)) != -1) {
		switch (opt) {
		case 'f':
			file = optarg;
			break;
		case 'V':
			print_version();
			return EXIT_SUCCESS;
		case 'H':
			print_help();
			return EXIT_SUCCESS;
		default:
			print_usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if (optind == argc) {
		print_usage(stderr);
		return EXIT_FAILURE;
	}

	if (file && !freopen(file, "r", stdin)) {
		perror(file);
		return EXIT_FAILURE;
	}

	for (i = optind; i < argc; i++) {
		const char *pattern = argv[i];

		argv[i] = (char *)glob_compile(pattern);
		if (!argv[i]) {
			fprintf(stderr, "%s: failed to compile pattern: %s\n",
			                progname, pattern);
			return EXIT_FAILURE;
		}
	}

	while ((rc = do_getline(&line, &sz)) > 0) {
		for (i = optind; i < argc; i++) {
			if (glob_match((void *)argv[i], line))
				break;
		}

		printf("%smatched,%s%s\n",
		       i < argc ? "" : "not ",
		       " " + !*line, line);
	}

	return rc < 0 ? EXIT_FAILURE : 0;
}
