/*
 * 2ooM: The Master of Orion II Reverse Engineering Project
 * Test program to verify behaviour accesing multiple archive members.
 *
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "error.h"
#include "lbx.h"
#include "tap.h"

/* A simple archive containing two files with two bytes each */
static const unsigned char testdata[] = {
	0x02, 0x00, 0xad, 0xfe, 0x00, 0x00, 0x00, 0x00,
	0x14, 0x00, 0x00, 0x00, 0x16, 0x00, 0x00, 0x00,
	0x18, 0x00, 0x00, 0x00, 0xde, 0xad, 0xbe, 0xef
};

static void check_offset(FILE *f, unsigned exp)
{
	long pos;

	pos = ftell(f);
	if (!tap_result(pos == exp, "file offset")) {
		tap_diag("Failed, unexpected result");
		if (pos < 0)
			tap_diag("   Received: %ld (%s)", pos, strerror(errno));
		else
			tap_diag("   Received: %ld", pos);
		tap_diag("   Expected: %u", exp);
	}
}

static void check_seek(LBXfile *f, long pos)
{
	long rc;

	rc = lbx_file_seek(f, pos, SEEK_SET);
	if (!tap_result(rc == 0, "lbx_file_seek(%p)", (void *)f)) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %ld (%s)", rc, lbx_errmsg());
		tap_diag("   Expected: 0");
	}

	rc = lbx_file_tell(f);
	if (!tap_result(rc == pos, "lbx_file_tell(%p)", (void *)f)) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %ld", rc);
		tap_diag("   Expected: %ld", pos);
	}
}

static void dump_buf(const char *prefix, const unsigned char *buf, size_t n)
{
	int w = strlen(prefix);
	size_t i;

	for (i = 0; i+3 < n; n -= 4) {
		tap_diag("%*s %.2x %.2x %.2x %.2x", w, prefix,
		         buf[i], buf[i+1], buf[i+2], buf[i+3]);
		prefix = "";
	}

	switch (n) {
	case 1: tap_diag("%*s %.2x", w, prefix, buf[i]);
		break;
	case 2: tap_diag("%*s %.2x %.2x", w, prefix, buf[i], buf[i+1]);
		break;
	case 3: tap_diag("%*s %.2x %.2x %.2x", w, prefix, buf[i], buf[i+2]);
		break;
	}
}

static void check_read(LBXfile *lf, const unsigned char *exp, size_t n)
{
	char buf[100];
	size_t rc;

	rc = lbx_file_read(lf, buf, 100);
	if (!tap_result(rc == n, "lbx_file_read(%p) -> %lu", (void *)lf,
	                         (unsigned long)rc))
	{
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %lu", (unsigned long)rc);
		tap_diag("   Expected: %lu", (unsigned long)n);
	}

	if (!tap_result(memcmp(buf, exp, n) == 0, "read data")) {
		tap_diag("Failed, data mismatch");
		dump_buf("   Received:", buf, n);
		dump_buf("   Expected:", exp, n);
	}
}

static void check_eof(LBXfile *f, int exp)
{
	int rc;

	rc = lbx_file_eof(f);
	if (!tap_result(rc == exp, "lbx_file_eof(%p)", (void *)f)) {
		tap_diag("Failed, unexpected result");
		tap_diag("   Received: %d", rc);
		tap_diag("   Expected: %d", exp);
	}
}

int main(void)
{
	LBXfile *lf0, *lf1;
	struct lbx *lbx;
	FILE *f;
	long pos;

	if (!(f = tmpfile()))
		tap_bail_out("tmpfile failed: %s", strerror(errno));
	if (fwrite(testdata, sizeof testdata, 1, f) != 1)
		tap_bail_out("fwrite failed: %s", strerror(errno));
	if (fseek(f, 0, SEEK_SET) != 0)
		tap_bail_out("rewind failed: %s", strerror(errno));

	lbx = lbx_open(f, NULL, NULL, "tmpfile");
	if (!tap_result(lbx != NULL, "lbx_open"))
		tap_diag("Failed, lbx_open error: %s", lbx_errmsg());
	check_offset(f, 0x14);

	lf0 = lbx_file_open(lbx, 0);
	if (!tap_result(lf0 != NULL, "lbx_file_open(0) -> %p", (void *)lf0))
		tap_diag("Failed, lbx_file_open error: %s", lbx_errmsg());

	lf1 = lbx_file_open(lbx, 1);
	if (!tap_result(lf0 != NULL, "lbx_file_open(1) -> %p", (void *)lf1))
		tap_diag("Failed, lbx_file_open error: %s", lbx_errmsg());

	check_eof(lf0, 0);
	check_read(lf0, testdata+0x14, 2);
	check_eof(lf0, 1);

	check_eof(lf1, 0);
	check_read(lf1, testdata+0x16, 2);
	check_seek(lf0, 0);
	check_eof(lf0, 0);
	check_eof(lf1, 1);

	check_seek(lf1, 1);
	check_eof(lf1, 0);

	check_read(lf0, testdata+0x14, 2);
	check_eof(lf0, 1);
	check_eof(lf1, 0);
	check_read(lf1, testdata+0x17, 1);
	check_eof(lf1, 1);

	tap_done();
}
