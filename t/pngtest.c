/*
 * 2ooM: The Master of Orion II Reverse Engineering Project
 * PNG conversion helper for testing.
 *
 * Copyright © 2024 Nick Bowler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <png.h>
#include <setjmp.h>

#include "help.h"
#include "gnu_getopt.h"

static const char *progname = "pngtest";
static const char sopts[] = "VH";
static const struct option lopts[] = {
	{ "version", 0, 0, 'V' },
	{ "help", 0, 0, 'H' },
	{0}
};

static void print_version(void)
{
	puts("\
pngtest (" PACKAGE_NAME ") " PACKAGE_VERSION "\n\
Copyright (C) 2024 Nick Bowler.\n\
License GPLv3+: GNU General Public License version 3 or any later version.\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.");
}

static void print_usage(FILE *f)
{
	fprintf(f, "Usage: %s file1.png [file2.png ...]\n", progname);
}

static void print_help(void)
{
	const struct option *opt;

	print_usage(stdout);
	puts("Decode PNG files for testing.\n");
	puts("Options:");
        for (opt = lopts; opt->val; opt++) {
                if (help_print_optstring(opt, "ARG", 20))
                        putchar('\n');
        }
}

static png_structp init_libpng(png_infop *info)
{
	png_structp png;

	if (!(png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0)))
		return NULL;

	if (!(*info = png_create_info_struct(png)))
		png_destroy_read_struct(&png, NULL, NULL);

	return png;
}

/*
 * Create an output filename from an input filename and desired file extension.
 *
 * This is constructed by deleting the .png suffix (if any) present on ifname
 * and then appending ext.
 *
 * If successful, the returned pointer refers to allocated storage which must
 * be freed by the caller.  Otherwise, an error message is printed and a null
 * pointer is returned.
 */
static char *mk_ofname(const char *ifname, const char *ext)
{
	size_t n = strlen(ifname);
	char *ret;

	if (!(ret = malloc(n + strlen(ext) + 1))) {
		fprintf(stderr, "%s: failed to allocate memory\n", progname);
		return NULL;
	}

	if (n > 4 && (ifname[n-3] == 'P' || ifname[n-3] == 'p')
	          && (ifname[n-2] == 'N' || ifname[n-2] == 'n')
	          && (ifname[n-1] == 'G' || ifname[n-1] == 'g')
		  && (ifname[n-4] == '.'))
	{
		n -= 4;
	}

	strcpy(&ret[n], ext);
	return memcpy(ret, ifname, n);
}

/*
 * Helper to open an output file.
 *
 * First, the output filename is generated via mk_ofname(ifname, ext),
 * then the file is opened for writing.
 *
 * If successful, the open file handle is stored in *of and the filename
 * is returned.  Both values should be passed to of_end to close the file.
 *
 * Otherwise, an error message is printed and a null pointer is returned.
 */
static char *of_start(FILE **of, const char *ifname, const char *ext)
{
	char *ofname;

	if (!(ofname = mk_ofname(ifname, ext)))
		return NULL;

	if (!(*of = fopen(ofname, "w"))) {
		fprintf(stderr, "%s: failed to open %s: %s\n",
		                progname, ofname, strerror(errno));
		free(ofname);
		return NULL;
	}

	return ofname;
}

/*
 * Helper to close an output file.
 *
 * If error is non-zero, an error message is printed indicating that there
 * was an output failure and including the text of strerror(errno).
 *
 * In any case, the of handle is closed and the ofname string is freed.
 * If fclose(of) failed and we did not already print an error message above,
 * then a similar error message is printed.
 *
 * Returns 0 if no error is indicated, otherwise returns -1.
 */
static int of_end(FILE *of, char *ofname, int error)
{
	int my_errno = 0;

	if (error)
		my_errno = errno;

	if (fclose(of) == EOF && !error++)
		my_errno = errno;

	if (error) {
		fprintf(stderr, "%s: %s: write failed: %s\n",
		                progname, ofname, strerror(my_errno));
	} else {
		printf("wrote %s\n", ofname);
	}
	free(ofname);

	return -(error != 0);
}
#define of_error(of, ofname) (of_end(of, ofname, 1))
#define of_done(of, ofname) (of_end(of, ofname, 0))

/*
 * Convert the intensity component of a gray (depth=1) or gray+alpha (depth=2)
 * PNG to plain PGM format.
 *
 * This should be identical to what is produced by the native PPM writer
 * for the same (other than format selection) lbximg invocation that
 * produced the input PNG.
 */
static int convert_g(const char *ifname, png_bytepp rows,
                     unsigned long height, unsigned long width,
                     unsigned depth)
{
	unsigned long x, y;
	char *ofname;
	FILE *of;

	if (!(ofname = of_start(&of, ifname, ".ppm")))
		return -1;
	if (fprintf(of, "P2\n%lu %lu\n255\n", width, height) < 0)
		goto err;

	for (y = 0; y < height; y++) {
		png_bytep row = rows[y];

		for (x = 0; x < width; x++) {
			if (fprintf(of, " %3d" + !x, *row) < 0)
				goto err;
			row += depth;
		}

		if (putc('\n', of) == EOF)
			goto err;
	}

	return of_done(of, ofname);
err:
	return of_error(of, ofname);
}

/*
 * Convert the alpha component of a gray+alpha (depth=2) or RGB+alpha (depth=4)
 * PNG to plain PBM format.
 *
 * This should be identical to what is produced by the native PBM writer for
 * the same (other than format selection) lbximg invocation that produced the
 * input PNG.
 *
 * It is an error for any pixel to have an alpha value other than 0 or 255.
 */
static int convert_a(const char *ifname, png_bytepp rows,
                     unsigned long height, unsigned long width,
		     unsigned depth)
{
	unsigned long x, y;
	int alpha_ret = 0;
	char *ofname;
	FILE *of;

	if (!(ofname = of_start(&of, ifname, ".pbm")))
		return -1;
	if (fprintf(of, "P1\n%lu %lu\n", width, height) < 0)
		goto err;

	for (y = 0; y < height; y++) {
		png_bytep row = rows[y] + (depth-1);

		for (x = 0; x < width; x++) {
			switch (*row) {
			case 255: case 0: break;
			default: alpha_ret = -1;
			}

			if (fputs(" 0\0\0 1" + (!*row << 2) + !x, of) < 0)
				goto err;
			row += depth;
		}

		if (putc('\n', of) == EOF)
			goto err;
	}

	if (alpha_ret) {
		fprintf(stderr, "%s: %s: invalid (semitransparent) alpha\n",
		                progname, ifname);
	}

	if (of_done(of, ofname) == 0)
		return alpha_ret;
	return -1;
err:
	return of_error(of, ofname);
}

/*
 * Convert the colour components of an RGB (depth=3) or RGB+alpha (depth=4)
 * PNG to plain PPM format.
 *
 * This should be identical to what is produced by the native PPM writer
 * for the same (other than format selection) lbximg invocation that
 * produced the input PNG.
 */
static int convert_rgb(const char *ifname, png_bytepp rows,
                       unsigned long height, unsigned long width,
		       unsigned depth)
{
	unsigned long x, y;
	char *ofname;
	FILE *of;

	if (!(ofname = of_start(&of, ifname, ".ppm")))
		return -1;
	if (fprintf(of, "P3\n%lu %lu\n63\n", width, height) < 0)
		goto err;

	for (y = 0; y < height; y++) {
		png_bytep row = rows[y];

#define RGB(row) ((row)[0]/4u),((row)[1]/4u),((row)[2]/4u)
		for (x = 0; x < width; x++) {
			if (fprintf(of, " %2u %2u %2u" + !x, RGB(row)) < 0)
				goto err;
			row += depth;
		}

		if (putc('\n', of) == EOF)
			goto err;
	}

	return of_done(of, ofname);
err:
	return of_error(of, ofname);
}

static int convert_ga(const char *ifname, png_bytepp rows,
                      unsigned long height, unsigned long width,
		      unsigned depth)
{
	int ret = 0;

	if (convert_g(ifname, rows, height, width, depth) != 0)
		ret = -1;
	if (convert_a(ifname, rows, height, width, depth) != 0)
		ret = -1;

	return ret;
}

static int convert_rgba(const char *ifname, png_bytepp rows,
                        unsigned long height, unsigned long width,
			unsigned depth)
{
	int ret = 0;

	if (convert_rgb(ifname, rows, height, width, depth) != 0)
		ret = -1;
	if (convert_a(ifname, rows, height, width, depth) != 0)
		ret = -1;

	return ret;
}

static int convert_png(const char *ifname, FILE *f,
                       png_structp png, png_infop info)
{
	unsigned long height, width;
	char *ofname, *ext;
	png_bytepp rows;
	unsigned depth;
	FILE *of;

	if (setjmp(png_jmpbuf(png)))
		return -1;

	png_init_io(png, f);
	png_read_png(png, info, PNG_TRANSFORM_EXPAND|PNG_TRANSFORM_SCALE_16, 0);
	height = png_get_image_height(png, info);
	width = png_get_image_width(png, info);
	rows = png_get_rows(png, info);

	switch ((depth = png_get_channels(png, info))) {
	case 1: return convert_g(ifname, rows, height, width, depth);
	case 2: return convert_ga(ifname, rows, height, width, depth);
	case 3: return convert_rgb(ifname, rows, height, width, depth);
	case 4: return convert_rgba(ifname, rows, height, width, depth);
	}

	return -1;
}

static int convert_file(const char *ifname)
{
	png_structp png;
	png_infop info;
	FILE *f;
	int ret;

	if (!(f = fopen(ifname, "rb"))) {
		fprintf(stderr, "%s: failed to open %s: %s\n",
		                progname, ifname, strerror(errno));
		return -1;
	}

	png = init_libpng(&info);
	ret = convert_png(ifname, f, png, info);
	png_destroy_read_struct(&png, &info, NULL);
	fclose(f);

	return ret;
}

int main(int argc, char **argv)
{
	int i, opt, ret;

	if (argc > 0)
		progname = argv[0];

	while ((opt = getopt_long(argc, argv, sopts, lopts, NULL)) != -1) {
		switch (opt) {
		case 'V':
			print_version();
			return EXIT_SUCCESS;
		case 'H':
			print_help();
			return EXIT_SUCCESS;
		default:
			print_usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if (optind == argc) {
		fprintf(stderr, "%s: no file specified\n", progname);
		print_usage(stderr);
		return EXIT_FAILURE;
	}

	ret = EXIT_SUCCESS;
	for (i = optind; i < argc; i++) {
		if (convert_file(argv[i]) != 0)
			ret = EXIT_FAILURE;
	}

	return ret;
}
