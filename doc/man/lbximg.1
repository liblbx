.Dd September 6, 2024
.Dt LBXIMG \&1 "2ooM Reference Manual"
.Os liblbx
.Sh NAME
.Nm lbximg
.Nd inspect and decode LBX images
.Sh SYNOPSIS
.Nm
.Op Fl i Ns | Ns Fl d
.Op Fl v
.Op Fl n
.Op Fl F Ar format
.Op Fl p Ar palette_file
.Op Fl O Ar override_file
.Op Fl f Ar path
.Op Ar frameno ...
.Sh DESCRIPTION
.Nm
identifies and decodes LBX image files, using
.Em liblbx .
LBX images are multi-frame, 256-colour paletted images with transparency.
.Nm
can be used to convert some or all of the frames of an LBX image to other image
formats.
.Sh OPTIONS
.Bl -tag -width indent
.It Fl i , \-ident
Sets the operating mode to identify the image format.
.It Fl d , \-decode
Sets the operating mode to decode frames to PNG.
.It Fl v , \-verbose
Output additional information on standard output.
.It Fl F , \-format Ar format
Selects the output format from the table below.
Possible values of
.Ar format
are:
.Bl -tag "pam"
.It pam Ta
Output images in Netpbm PAM format, an uncompressed raster image format
supporting 18-bit RGB (6 bits per component) with transparency.
This is the default.
.It ppm Ta
Output colour information in Netpbm "plain" PPM format, a 7-bit clean image
format supporting 18-bit RGB but not transparency.
Transparent pixels are output as black.
This format is rather inefficient and provided mainly for testing.
However, it is supported by more image processing tools than the PAM format.
.It pbm Ta
Output transparency information in Netpbm "plain" PBM format, to complement
the PPM output format.
Black (1) pixels are transparent, white (0) pixels are opaque.
This format is rather inefficient and provided mainly for testing.
However, it is supported by more image processing tools than the PAM format.
.It png Ta
Output images in Portable Network Graphics (PNG) format.
This is an efficient format supporting RGB and transparency with very good
compatibility with other tools, but does not support 18-bit RGB so colour
values are widened to 24 bits.
This transformation can be reversed by dividing each component value by 4.
.El
.It Fl f , \-file Ar path
Read from the specified
.Ar path
instead of standard input.
.It Fl \-output\-prefix Ar string
Prefix each output filename with the given string.
Filenames are produced by appending a 3-digit frame number and file extension
to the specified prefix.
The default prefix is
.Li out_ .
.It Fl n , \-no\-palette
Instead of looking up colour indices in the palette, emit an 8-bit grayscale
image where the intensity value of a pixel is equal to the palette index.
This is mainly useful for debugging the image decoder in
.Nm liblbx .
.It Fl p , \-palette Ar palette_file
Read the base palette from
.Ar palette_file .
In Moo2, these files are found in
.Pa fonts.lbx .
By default, all palette entries are initially pink.
The base palette entries are replaced by embedded palette entries in the
main image file or any specified override image files.
.It Fl O , \-override Ar override_file
Load additional embedded palette entries from the image specified by
.Ar override_file .
Palette entries from the override image entries take priority over both the
base and main image palettes.
This option can be specified multiple times to combine palettes from multiple
images.
In Moo2, this function is used to colourise to ship images.
.It Fl V , \-version
Print a version message and exit.
.It Fl \-usage
Print a short usage message and exit.
.It Fl \-help
Print a help message and exit
.It Ar frameno ...
The remaining arguments specify which frames from an animation to decode.
If nothing is specified, then all frames are decoded.
Otherwise, each argument is of the form
.Ar start Ns Op \- Ns Op Ar end
to indicate an inclusive range of frames from
.Ar start
to
.Ar end .
If
.Ar end
is omitted, then the range continues to the last frame.
If the hyphen is omitted, then only the
.Ar start
frame is selected.
.Pp
For example:
.Bl -tag -width
.It Nm Li \-d 3 6
Decode frames 3 and 6.
.It Nm Li \-d 4\-9
Decode frames 4 through 9, inclusive
.It Nm Li \-d 6\-9 1\-4
Decode frames 1 through 9, except for frame 5.
.It Nm Li \-d 3\-
Decode all frames except the first 3.
.El
.El
.Sh EXAMPLES
After extracting fonts.lbx, mainmenu.lbx, logo.lbx and ships.lbx with
.Xr lbxtool 1 :
.Bl -tag -width indent
.It Nm Li \-ivf logo.lbx.000
Print some information about the Simtex logo image.
.It Nm Li \-dvf mainmenu.lbx.000 \-p fonts.lbx.006
Decodes all the frames of the main menu panel into a series of PNGs.
.It Nm Li \-dvf logo.lbx.001
Decodes all the frames of the Microprose logo into a series of PNGs.
.It Nm Li \-dvf ships.lbx.042 \-p fonts.lbx.012 \-O ships.lbx.049
Decodes an image of a red star fortress.
.El
.Sh AUTHORS
Nick Bowler <nbowler@draconx.ca>
.Sh COPYRIGHT
Copyright \(co 2008, 2010, 2013, 2021, 2023\(en2024 Nick Bowler
.Pp
Permission is granted to copy, distribute and/or modify this manual under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
.Sh SEE ALSO
.Xr lbxtool 1 ,
.Xr lbxgui 1
