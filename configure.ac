DX_COPYRIGHT([GPLv3+],
# Copyright (C) 2009, 2013-2024, 2021, 2023-2024 Nick Bowler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
)

DX_PATCH_LIBTOOL

AC_PREREQ([2.68])
AC_INIT([liblbx], [0.1], [nbowler@draconx.ca])
AC_CONFIG_SRCDIR([src/lbx.c])
AC_CONFIG_HEADERS([config.h])

AM_INIT_AUTOMAKE([-Wall -Wno-portability foreign subdir-objects])
AM_SILENT_RULES([no])
DX_AUTOMAKE_COMPAT

AC_PROG_CC_C99
AM_PROG_CC_C_O

AC_C_INLINE
AC_C_FLEXIBLE_ARRAY_MEMBER
AC_HEADER_ASSERT
DX_C99TYPES

LT_INIT

AC_ARG_WITH([libpng],
  [AS_HELP_STRING([--with-libpng],
    [build support for PNG output using libpng. [default=auto]])],
  [with_libpng=$withval], [with_libpng=auto])

AS_CASE([$with_libpng],
  [no], [have_libpng=false],
  [yes], [DX_LIB_LIBPNG([1.5], [have_libpng=true])],
  [DX_LIB_LIBPNG([1.5], [have_libpng=true], [have_libpng=false])])

AM_CONDITIONAL([HAVE_LIBPNG], [$have_libpng])
AS_IF([$have_libpng], [AC_DEFINE([HAVE_LIBPNG], [1],
  [Define to 1 if libpng support is available])])

AC_ARG_ENABLE([lbxgui],
  [AS_HELP_STRING([--enable-lbxgui],
    [build the lbxgui tool (requires GTK+) [default=auto]])],
  [enable_lbxgui=$enableval],
  [enable_lbxgui=auto])

AS_CASE([$enable_lbxgui],
  [no], [have_gtk=false],
  [yes], [DX_LIB_GTK2([2.16], [have_gtk=true])],
  [DX_LIB_GTK2([2.16], [have_gtk=true], [have_gtk=false])])

AM_CONDITIONAL([BUILD_LBXGUI], [$have_gtk])

# Check for utilities used by the test suite.
DX_PROG_MD5

AC_CONFIG_TESTDIR([.], [t:.])
DX_PROG_AUTOTEST_AM
DX_CHECK_GETOPT_LONG_AM

AC_CONFIG_FILES([Makefile])
AC_OUTPUT
